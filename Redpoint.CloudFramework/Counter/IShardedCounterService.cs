﻿using Redpoint.CloudFramework.Datastore;
using Redpoint.CloudFramework.Models;
using System;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Counter
{
    public interface IShardedCounterService
    {
        /// <summary>
        /// Returns the value of a sharded counter.
        /// </summary>
        /// <param name="name">The name of the sharded counter.</param>
        /// <returns>The value of the sharded counter.</returns>
        Task<long> Get(string name);

        /// <summary>
        /// Returns the value of a sharded counter stored in a custom model.
        /// </summary>
        /// <typeparam name="T">The model that is used to store sharded counter data.</typeparam>
        /// <param name="name">The name of the sharded counter.</param>
        /// <returns>The value of the sharded counter.</returns>
        Task<long> GetCustom<T>(string name) where T : Model, IShardedCounterModel, new();

        /// <summary>
        /// Adjust the value of a sharded counter.
        /// </summary>
        /// <param name="name">The name of the sharded counter.</param>
        /// <param name="modifier">The amount to modify the sharded counter by.</param>
        /// <returns>The task to await on.</returns>
        Task Adjust(string name, long modifier);

        /// <summary>
        /// Adjust the value of a sharded counter stored in a custom model.
        /// </summary>
        /// <typeparam name="T">The model that is used to store sharded counter data.</typeparam>
        /// <param name="name">The name of the sharded counter.</param>
        /// <param name="modifier">The amount to modify the sharded counter by.</param>
        /// <returns>The task to await on.</returns>
        Task AdjustCustom<T>(string name, long modifier) where T : Model, IShardedCounterModel, new();

        /// <summary>
        /// Adjust the value of a sharded counter inside an existing transaction. You *must* await this
        /// function and call the callback it returns after you commit the provided transaction.
        /// </summary>
        /// <param name="name">The name of the sharded counter.</param>
        /// <param name="modifier">The amount to modify the sharded counter by.</param>
        /// <param name="existingTransaction">The existing transaction to update the counter in.</param>
        /// <returns>The task to await on.</returns>
        Task<Func<Task>> Adjust(string name, long modifier, ModelTransaction existingTransaction);

        /// <summary>
        /// Adjust the value of a sharded counter stored in a custom model, inside an existing transaction.
        /// You *must* await this function and call the callback it returns after you commit the provided transaction.
        /// </summary>
        /// <param name="name">The name of the sharded counter.</param>
        /// <param name="modifier">The amount to modify the sharded counter by.</param>
        /// <param name="existingTransaction">The existing transaction to update the counter in.</param>
        /// <returns>The task to await on.</returns>
        Task<Func<Task>> AdjustCustom<T>(string name, long modifier, ModelTransaction existingTransaction) where T : Model, IShardedCounterModel, new();
    }
}
