﻿using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Datastore;
using Redpoint.CloudFramework.Models;
using StackExchange.Redis;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Counter
{

    public class ShardedCounterService : IShardedCounterService
    {
        private readonly IGoogleDatastoreRepository _googleDatastoreRepository;
        private readonly ConnectionMultiplexer _connectionMultiplexer;
        private readonly IDatabase _database;
        private readonly Random _random;

        // This can only ever be increased; not decreased.
        public const int NumShards = 60;

        public ShardedCounterService(
            IGoogleDatastoreRepository googleDatastoreRepository,
            ConnectionMultiplexer connectionMultiplexer)
        {
            _googleDatastoreRepository = googleDatastoreRepository;
            _connectionMultiplexer = connectionMultiplexer;
            _database = _connectionMultiplexer.GetDatabase();
            _random = new Random();
        }

        private async Task<Key[]> GetAllKeys<T>(string name) where T : Model, IShardedCounterModel, new()
        {
            var t = new T();

            var keyFactory = await _googleDatastoreRepository.GetKeyFactory<T>();
            var keys = new Key[NumShards];
            for (var i = 0; i < NumShards; i++)
            {
                keys[i] = keyFactory.CreateKey(t.FormatShardName(name, i));
            }
            return keys;
        }

        public async Task AdjustCustom<T>(string name, long modifier) where T : Model, IShardedCounterModel, new()
        {
            var t = new T();

            var idx = _random.Next(0, NumShards);
            var keyFactory = await _googleDatastoreRepository.GetKeyFactory<T>();
            var key = keyFactory.CreateKey(t.FormatShardName(name, idx));
            var transaction = await _googleDatastoreRepository.BeginTransaction();
            var rollback = true;
            try
            {
                var create = false;
                var counter = await transaction.Transaction.LookupAsync(key);
                if (counter == null)
                {
                    counter = new Entity
                    {
                        Key = key,
                        Properties =
                        {
                            { t.GetCountFieldName(), modifier },
                        }
                    };
                    var typeName = t.GetTypeFieldName();
                    if (typeName != null)
                    {
                        counter.Properties[typeName] = "shard";
                    }
                    create = true;
                }
                else
                {
                    counter[t.GetCountFieldName()].IntegerValue += modifier;
                }
                if (create)
                {
                    transaction.Transaction.Insert(counter);
                }
                else
                {
                    transaction.Transaction.Update(counter);
                }
                await transaction.Transaction.CommitAsync();
                await _database.StringIncrementAsync("shard-" + name, modifier, CommandFlags.FireAndForget);
            }
            finally
            {
                if (rollback)
                {
                    await transaction.Transaction.RollbackAsync();
                }
            }
        }


        public async Task<Func<Task>> AdjustCustom<T>(string name, long modifier, ModelTransaction existingTransaction) where T : Model, IShardedCounterModel, new()
        {
            var t = new T();

            var idx = _random.Next(0, NumShards);
            var keyFactory = await _googleDatastoreRepository.GetKeyFactory<T>();
            var key = keyFactory.CreateKey(t.FormatShardName(name, idx));
            var create = false;
            var counter = await existingTransaction.Transaction.LookupAsync(key);
            if (counter == null)
            {
                counter = new Entity
                {
                    Key = key,
                    Properties =
                    {
                        { t.GetCountFieldName(), modifier },
                    }
                };
                var typeName = t.GetTypeFieldName();
                if (typeName != null)
                {
                    counter.Properties[typeName] = "shard";
                }
                create = true;
            }
            else
            {
                counter[t.GetCountFieldName()].IntegerValue += modifier;
            }
            if (create)
            {
                existingTransaction.Transaction.Insert(counter);
            }
            else
            {
                existingTransaction.Transaction.Update(counter);
            }
            return async () =>
            {
                await _database.StringIncrementAsync("shard-" + name, modifier, CommandFlags.FireAndForget);
            };
        }

        public async Task<long> GetCustom<T>(string name) where T : Model, IShardedCounterModel, new()
        {
            var t = new T();

            long total;
            var shardCache = await _database.StringGetAsync("shard-" + name);
            if (!shardCache.HasValue || !shardCache.IsInteger || !shardCache.TryParse(out total))
            {
                var db = await _googleDatastoreRepository.GetDatastoreDb();
                var entities = await db.LookupAsync(await GetAllKeys<T>(name));
                total = entities.Where(x => x != null).Select(x => x[t.GetCountFieldName()].IntegerValue).Sum();
                await _database.StringSetAsync(
                    "shard-" + name,
                    total,
                    TimeSpan.FromSeconds(60),
                    When.NotExists);
                return total;
            }
            return total;
        }

        public Task<long> Get(string name)
        {
            return GetCustom<DefaultShardedCounterModel>(name);
        }

        public Task Adjust(string name, long modifier)
        {
            return AdjustCustom<DefaultShardedCounterModel>(name, modifier);
        }

        public Task<Func<Task>> Adjust(string name, long modifier, ModelTransaction existingTransaction)
        {
            return AdjustCustom<DefaultShardedCounterModel>(name, modifier, existingTransaction);
        }
    }
}
