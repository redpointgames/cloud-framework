﻿using Redpoint.CloudFramework.Models;

namespace Redpoint.CloudFramework.Datastore
{
    public interface IGoogleGlobalDatastoreModelMigration
    {
        T ApplyMigrations<T>(string @namespace, T model) where T : Model;
    }
}