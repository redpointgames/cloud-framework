﻿using System;

namespace Redpoint.CloudFramework.Datastore
{
    public class SchemaVersionAttribute : Attribute
    {
        public SchemaVersionAttribute(ulong version)
        {
            SchemaVersion = version;
        }

        public ulong SchemaVersion { get; private set; }
    }
}
