﻿using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Models;

namespace Redpoint.CloudFramework.Datastore
{
    public class ModelQuery<T> where T : Model, new()
    {
        public ModelQuery(string @namespace, Query query)
        {
            Namespace = @namespace;
            Query = query;
        }

        public string Namespace { get; }
        public Query Query { get; }
    }
}
