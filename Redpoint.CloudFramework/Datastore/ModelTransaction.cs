﻿using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Datastore
{
    public class ModelTransaction
    {
        public ModelTransaction(string @namespace, DatastoreTransaction transaction)
        {
            Namespace = @namespace;
            Transaction = transaction;
            ModifiedModels = new List<Model>();
            QueuedPreCommitOperations = new List<Func<Task>>();
        }

        /// <summary>
        /// The namespace the transaction is occurring in.
        /// </summary>
        public string Namespace { get; }

        /// <summary>
        /// The transaction itself.
        /// </summary>
        internal DatastoreTransaction Transaction { get; }

        /// <summary>
        /// A list of models that have been modified by this transaction.
        /// </summary>
        public List<Model> ModifiedModels { get; }

        /// <summary>
        /// A list of queued operations to be performed immediately before
        /// Commit() is called for Datastore.
        /// </summary>
        public List<Func<Task>> QueuedPreCommitOperations { get; }
    }
}
