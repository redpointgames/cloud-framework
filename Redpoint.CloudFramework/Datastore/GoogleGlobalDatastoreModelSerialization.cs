﻿using Google.Cloud.Datastore.V1;
using Google.Cloud.Diagnostics.Common;
using Google.Type;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NodaTime;
using Redpoint.CloudFramework.Models;
using Redpoint.CloudFramework.Prefix;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Redpoint.CloudFramework.Datastore
{
    public class GoogleGlobalDatastoreModelSerialization : IGoogleGlobalDatastoreModelSerialization
    {
        private readonly IGlobalPrefix _globalPrefix;
        private readonly IManagedTracer _managedTracer;
        private readonly ILogger<GoogleGlobalDatastoreModelSerialization> _logger;
        private readonly IGoogleGlobalDatastoreModelMigration _googleGlobalDatastoreModelMigration;

        public GoogleGlobalDatastoreModelSerialization(
            IGlobalPrefix globalPrefix,
            IManagedTracer managedTracer,
            ILogger<GoogleGlobalDatastoreModelSerialization> logger,
            IGoogleGlobalDatastoreModelMigration googleGlobalDatastoreModelMigration)
        {
            _globalPrefix = globalPrefix;
            _managedTracer = managedTracer;
            _logger = logger;
            _googleGlobalDatastoreModelMigration = googleGlobalDatastoreModelMigration;
        }

        public T FromJson<T>(string @namespace, string json) where T : Model, new()
        {
            return FromJsonCache<T>(@namespace, json);
        }

        private T FromJsonCache<T>(string @namespace, string jsonCache) where T : Model, new()
        {
            using (_managedTracer.StartSpan("DbSer:FromJsonCache<" + typeof(T).Name + ">()"))
            {
                var model = new T();
                model._originalData = new Dictionary<string, object>();

                var hashset = JObject.Parse(jsonCache);

                if (hashset["_isnull"]?.Value<bool>() ?? false)
                {
                    // The object does not exist (and we've cached the non-existence of it
                    // during a previous load).
                    return null;
                }

                var delayedLocalKeyAssignments = new List<Action<string>>();

                var types = model.GetTypes();
                foreach (var kv in types)
                {
                    var typeInfo = model.GetType().GetTypeInfo();
                    var propInfo = typeInfo.GetDeclaredProperty(kv.Key);
                    if (propInfo == null)
                    {
                        _logger.LogWarning("Model {0} declares property {1} but is missing C# declaration", typeof(T).FullName, kv.Key);
                        continue;
                    }

                    object value = null;
                    if (hashset[kv.Key].Type == JTokenType.Null)
                    {
                        // Preserve null.
                    }
                    else
                    {
                        switch (kv.Value)
                        {
                            case "string":
                                value = hashset[kv.Key]?.Value<string>();
                                break;
                            case "boolean":
                                value = hashset[kv.Key]?.Value<bool>();
                                break;
                            case "integer":
                                value = hashset[kv.Key]?.Value<long>();
                                break;
                            case "geopoint":
                                value = FromJsonCacheToLatLng(hashset[kv.Key]);
                                break;
                            case "key":
                                var keyValue = _globalPrefix.ParseInternal(@namespace, hashset[kv.Key]?.Value<string>());

                                if (keyValue.PartitionId.NamespaceId != @namespace)
                                {
                                    throw new InvalidOperationException("Unable to load property '" + kv.Key + "' from entity; cross-namespace reference detected");
                                }

                                value = keyValue;
                                break;
                            case "local-key":
                                var localKeyValue = _globalPrefix.ParseInternal(@namespace, hashset[kv.Key]?.Value<string>());

                                if (@namespace != string.Empty)
                                {
                                    throw new InvalidOperationException("local-key properties can not be used on entities outside the global namespace");
                                }

                                // We can't call GetDatastoreNamespaceForLocalKeys on the model until we've set all the
                                // other properties, since determining the Datastore namespace for local keys might 
                                // rely on other properties.
                                delayedLocalKeyAssignments.Add((localNamespace) =>
                                {
                                    if (localKeyValue.PartitionId.NamespaceId != localNamespace)
                                    {
                                        throw new InvalidOperationException("Unable to load property '" + kv.Key + "' from entity; cross-namespace reference detected");
                                    }

                                    propInfo.SetValue(model, localKeyValue);
                                    model._originalData[kv.Key] = localKeyValue;
                                });
                                value = null;
                                break;
                            case "global-key":
                                var globalKeyValue = _globalPrefix.ParseInternal(string.Empty, hashset[kv.Key]?.Value<string>());

                                if (@namespace == string.Empty)
                                {
                                    throw new InvalidOperationException("global-key properties can not be used on entities inside the global namespace");
                                }
                                if (globalKeyValue != null && globalKeyValue.PartitionId.NamespaceId != string.Empty)
                                {
                                    throw new InvalidOperationException("Unable to load property '" + kv.Key + "' from entity; cross-namespace reference detected");
                                }

                                value = globalKeyValue;
                                break;
                            case "unsafe-key":
                                value = _globalPrefix.ParseInternal(string.Empty, hashset[kv.Key]?.Value<string>());
                                break;
                            case "timestamp":
                                value = FromJsonCacheToNodaTimeInstant(hashset[kv.Key]);
                                break;
                            case "json":
                                {
                                    var rawJson = hashset[kv.Key]?.Value<string>();
                                    value = JsonConvert.DeserializeObject(rawJson, propInfo.PropertyType, new VersionedJsonConverter(), new NodaTimeInstantJsonConverter());
                                    break;
                                }
                            default:
                                throw new InvalidOperationException("Invalid type defined for property: " + kv.Value);
                        }
                    }

                    propInfo.SetValue(model, value);

                    model._originalData[kv.Key] = value;
                }

                model.Key = _globalPrefix.ParseInternal(@namespace, hashset["_key"]?.Value<string>());
                model.dateCreatedUtc = FromJsonCacheToNodaTimeInstant(hashset["_dateCreatedUtc"]);
                model.dateModifiedUtc = FromJsonCacheToNodaTimeInstant(hashset["_dateModifiedUtc"]);
                model.schemaVersion = hashset["_schemaVersion"]?.Value<int>();
                model.hasImplicitMigrationsApplied = hashset["_hasImplicitMigrationsApplied"]?.Value<bool>() ?? false;

                // If we have any delayed local key assignments, run them now (before migrations, in case
                // migrations want to handle local-key properties).
                if (delayedLocalKeyAssignments.Count > 0)
                {
                    var localNamespace = model.GetDatastoreNamespaceForLocalKeys();
                    foreach (var f in delayedLocalKeyAssignments)
                    {
                        f(localNamespace);
                    }
                }

                // Apply schema migrations where needed.
                model = _googleGlobalDatastoreModelMigration.ApplyMigrations(@namespace, model);

                return model;
            }
        }

        public string ToJson<T>(string @namespace, T model) where T : Model, new()
        {
            return ToJsonCache(model);
        }

        private string ToJsonCache<T>(T model) where T : Model, new()
        {
            using (_managedTracer.StartSpan("DbSer:ToJsonCache<" + typeof(T).Name + ">()"))
            {
                var hashset = new JObject();

                if (model == null)
                {
                    hashset.Add("_isnull", true);
                }
                else
                {
                    var types = model.GetTypes();
                    foreach (var kv in types)
                    {
                        var typeInfo = model.GetType().GetTypeInfo();
                        var propInfo = typeInfo.GetDeclaredProperty(kv.Key);
                        if (propInfo == null)
                        {
                            _logger.LogWarning("Model {0} declares property {1} but is missing C# declaration", typeof(T).FullName, kv.Key);
                            continue;
                        }

                        var value = propInfo.GetValue(model);

                        if (value == null)
                        {
                            hashset.Add(kv.Key, JValue.CreateNull());
                        }
                        else
                        {
                            switch (kv.Value)
                            {
                                case "string":
                                    hashset.Add(kv.Key, new JValue((string)value));
                                    break;
                                case "boolean":
                                    hashset.Add(kv.Key, new JValue((bool)value));
                                    break;
                                case "integer":
                                    hashset.Add(kv.Key, new JValue((long)value));
                                    break;
                                case "geopoint":
                                    hashset.Add(kv.Key, FromLatLngToJsonCache((LatLng)value));
                                    break;
                                case "key":
                                    var keyValue = (Key)value;

                                    if (keyValue.PartitionId.NamespaceId != model.Key.PartitionId.NamespaceId)
                                    {
                                        throw new InvalidOperationException("Attempted to store cross-namespace key reference in 'key' property");
                                    }

                                    hashset.Add(kv.Key, _globalPrefix.CreateInternal(keyValue));
                                    break;
                                case "local-key":
                                    var localValue = (Key)value;

                                    if (model.Key.PartitionId.NamespaceId != string.Empty)
                                    {
                                        throw new InvalidOperationException("Attempted to use 'local-key' in entity that is not in the global namespace");
                                    }

                                    if (localValue.PartitionId.NamespaceId != model.GetDatastoreNamespaceForLocalKeys())
                                    {
                                        throw new InvalidOperationException("Value for 'local-key' is not a key referencing an entity in the expected non-global namespace");
                                    }

                                    hashset.Add(kv.Key, _globalPrefix.CreateInternal(localValue));
                                    break;
                                case "global-key":
                                    var globalValue = (Key)value;

                                    if (model.Key.PartitionId.NamespaceId == string.Empty)
                                    {
                                        throw new InvalidOperationException("Attempted to use 'global-key' in entity that is in the global namespace");
                                    }

                                    if (globalValue.PartitionId.NamespaceId != string.Empty)
                                    {
                                        throw new InvalidOperationException("Value for 'global-key' is not a key referencing an entity in the global namespace");
                                    }

                                    hashset.Add(kv.Key, _globalPrefix.CreateInternal(globalValue));
                                    break;
                                case "unsafe-key":
                                    hashset.Add(kv.Key, _globalPrefix.CreateInternal((Key)value));
                                    break;
                                case "timestamp":
                                    hashset.Add(kv.Key, FromNodaTimeInstantToJsonCache((Instant?)value));
                                    break;
                                case "json":
                                    hashset.Add(kv.Key, JsonConvert.SerializeObject(value, new VersionedJsonConverter(), new NodaTimeInstantJsonConverter()));
                                    break;
                                default:
                                    throw new InvalidOperationException("Invalid type defined for property: " + kv.Value);
                            }
                        }
                    }

                    hashset.Add("_key", _globalPrefix.CreateInternal(model.Key));
                    hashset.Add("_dateCreatedUtc", FromNodaTimeInstantToJsonCache(model.dateCreatedUtc));
                    hashset.Add("_dateModifiedUtc", FromNodaTimeInstantToJsonCache(model.dateModifiedUtc));
                    hashset.Add("_schemaVersion", model.schemaVersion);
                    hashset.Add("_hasImplicitMigrationsApplied", model.hasImplicitMigrationsApplied);
                }

                return hashset.ToString();
            }
        }

        private Instant? FromJsonCacheToNodaTimeInstant(JToken obj)
        {
            if (obj == null || obj?.Type == JTokenType.Null)
            {
                return null;
            }

            return Instant.FromUnixTimeSeconds(obj["seconds"]?.Value<long>() ?? 0).PlusNanoseconds(obj["nanos"]?.Value<long>() ?? 0);
        }

        private JToken FromNodaTimeInstantToJsonCache(Instant? instant)
        {
            if (instant == null)
            {
                return JValue.CreateNull();
            }

            var seconds = instant.Value.ToUnixTimeSeconds();
            var nanos = (instant.Value - Instant.FromUnixTimeSeconds(instant.Value.ToUnixTimeSeconds())).SubsecondNanoseconds;
            var obj = new JObject();
            obj["seconds"] = (long)seconds;
            obj["nanos"] = (long)nanos;
            return obj;
        }

        private LatLng FromJsonCacheToLatLng(JToken obj)
        {
            if (obj == null || obj?.Type == JTokenType.Null)
            {
                return null;
            }

            return new LatLng
            {
                Latitude = obj["lat"]?.Value<double>() ?? 0,
                Longitude = obj["long"]?.Value<double>() ?? 0,
            };
        }

        private JToken FromLatLngToJsonCache(LatLng geopoint)
        {
            if (geopoint == null)
            {
                return JValue.CreateNull();
            }

            var obj = new JObject();
            obj["lat"] = (double)geopoint.Latitude;
            obj["long"] = (double)geopoint.Longitude;
            return obj;
        }
    }
}
