﻿using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Models;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Datastore
{
    public interface IGoogleGlobalDatastoreRepositoryHook
    {
        Task PostCreate<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new();
        Task PostUpsert<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new();
        Task PostUpdate<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new();
        Task PostDelete<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new();

        Task MutateEntityBeforeWrite(string @namespace, Entity entity);
    }
}
