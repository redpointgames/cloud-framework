﻿namespace Redpoint.CloudFramework.Datastore
{
    public enum MigrationOutcome
    {
        /// <summary>
        /// Migrating from the old version to the new version was completed implicitly.
        /// </summary>
        ImplicitMigrationsApplied,

        /// <summary>
        /// This object requires an explicit migration to occur before the schema version can be
        /// updated, usually by some background process. If you return this value, the datastore
        /// repository will fire a "migrate" event with this object attached.
        /// </summary>
        ExplicitMigrationRequired
    }
}
