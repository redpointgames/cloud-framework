﻿using Redpoint.CloudFramework.Models;

namespace Redpoint.CloudFramework.Datastore
{
    public interface IGoogleGlobalDatastoreModelSerialization
    {
        T FromJson<T>(string @namespace, string json) where T : Model, new();

        string ToJson<T>(string @namespace, T model) where T : Model, new();
    }
}
