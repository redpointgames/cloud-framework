﻿using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Options;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Datastore
{
    public class DistributedCacheExtended : IDistributedCacheExtended, IDisposable
    {
        private const string ClearCacheLuaScript =
            "for _,k in ipairs(redis.call('KEYS', ARGV[1])) do\n" +
            "    redis.call('DEL', k)\n" +
            "end";
        private const string GetKeysLuaScript = "return redis.call('keys', ARGV[1])";
        private readonly RedisCacheOptions options;
        private ConnectionMultiplexer connection;
        private IDatabase cache;
        private bool isDisposed;

        public DistributedCacheExtended(IOptions<RedisCacheOptions> redisCacheOptions)
        {
            this.options = redisCacheOptions.Value;
        }

        ~DistributedCacheExtended()
        {
            this.Dispose(false);
        }

        public async Task ClearAsync()
        {
            this.ThrowIfDisposed();
            await this.EnsureInitialized();
            await this.cache.ScriptEvaluateAsync(
                ClearCacheLuaScript,
                values: new RedisValue[]
                {
                this.options.InstanceName + "*"
                });
        }

        public async Task<IEnumerable<string>> GetKeysAsync()
        {
            this.ThrowIfDisposed();
            await this.EnsureInitialized();
            var result = await this.cache.ScriptEvaluateAsync(
                GetKeysLuaScript,
                values: new RedisValue[]
                {
                this.options.InstanceName + "*"
                });
            return ((RedisResult[])result).Select(x => x.ToString().Substring(this.options.InstanceName.Length)).ToArray();
        }

        public async Task RemoveAsync(string[] keys)
        {
            this.ThrowIfDisposed();
            if (keys == null) { throw new ArgumentNullException(nameof(keys)); }
            await this.EnsureInitialized();
            var keysArray = keys.Select(x => (RedisKey)(this.options.InstanceName + x)).ToArray();
            await this.cache.KeyDeleteAsync(keysArray);
        }

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected async Task EnsureInitialized()
        {
            if (connection == null)
            {
                this.connection = await ConnectionMultiplexer.ConnectAsync(this.options.Configuration);
                this.cache = this.connection.GetDatabase();
            }
        }

        private void Dispose(bool disposing)
        {
            if (!this.isDisposed)
            {
                if (disposing && this.connection != null)
                {
                    this.connection.Close();
                }

                this.isDisposed = true;
            }
        }

        private void ThrowIfDisposed()
        {
            if (this.isDisposed)
            {
                throw new ObjectDisposedException(nameof(DistributedCacheExtended));
            }
        }
    }

    public interface IDistributedCacheExtended
    {
        Task ClearAsync();
        Task<IEnumerable<string>> GetKeysAsync();
        Task RemoveAsync(string[] keys);
    }

    public class NullDistributedCacheExtended : IDistributedCacheExtended
    {
        public Task ClearAsync()
        {
            return Task.CompletedTask;
        }

        public async Task<IEnumerable<string>> GetKeysAsync()
        {
            return new string[0];
        }

        public Task RemoveAsync(string[] keys)
        {
            return Task.CompletedTask;
        }
    }
}
