﻿using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Datastore
{
    public class GoogleDatastoreRepository : IGoogleDatastoreRepository
    {
        private readonly IGoogleGlobalDatastoreRepository _globalDatastore;

        private readonly ICurrentTenantService _currentProject;

        public GoogleDatastoreRepository(IGoogleGlobalDatastoreRepository globalDatastore, ICurrentTenantService currentSite)
        {
            _globalDatastore = globalDatastore;
            _currentProject = currentSite;
        }

        private async Task<string> GetDatastoreNamespace()
        {
            return (await _currentProject.GetTenant()).DatastoreNamespace;
        }

        public async Task<DatastoreDb> GetDatastoreDb()
        {
            return _globalDatastore.GetDatastoreDbForNamespace(await GetDatastoreNamespace());
        }

        public async Task<ModelQuery<T>> CreateQuery<T>() where T : Model, new()
        {
            return await _globalDatastore.CreateQuery<T>(await GetDatastoreNamespace());
        }

        public async Task<MappedDatastoreQueryResults<T>> RunUncachedQuery<T>(ModelQuery<T> query,
            ReadOptions.Types.ReadConsistency readConsistency, ModelTransaction transaction) where T : Model, new()
        {
            return await _globalDatastore.RunUncachedQuery(await GetDatastoreNamespace(), query, readConsistency, transaction);
        }

        public async Task<KeyFactory> GetKeyFactory(string kind)
        {
            return await _globalDatastore.GetKeyFactory(await GetDatastoreNamespace(), kind);
        }

        public async Task<KeyFactory> GetKeyFactory<T>() where T : Model, new()
        {
            return await _globalDatastore.GetKeyFactory<T>(await GetDatastoreNamespace());
        }

        public async Task<Dictionary<Key, T>> LoadMany<T>(Key[] keys, ModelTransaction transaction = null) where T : Model, new()
        {
            return await _globalDatastore.LoadMany<T>(await GetDatastoreNamespace(), keys, transaction);
        }

        public async Task<T> LoadOneBy<T>(Key key, ModelTransaction transaction) where T : Model, new()
        {
            return await _globalDatastore.LoadOneBy<T>(await GetDatastoreNamespace(), key, transaction);
        }

        public async Task<T> LoadOneBy<T, U>(string field, U value, ModelTransaction transaction) where T : Model, new()
        {
            return await _globalDatastore.LoadOneBy<T, U>(await GetDatastoreNamespace(), field, value, transaction);
        }

        public async Task<List<T>> LoadAllBy<T, U>(string field, U value, ModelTransaction transaction) where T : Model, new()
        {
            return await _globalDatastore.LoadAllBy<T, U>(await GetDatastoreNamespace(), field, value, transaction);
        }

        public async Task<List<T>> LoadAll<T>(ModelTransaction transaction) where T : Model, new()
        {
            return await _globalDatastore.LoadAll<T>(await GetDatastoreNamespace(), transaction);
        }

        public async Task<List<T>> LoadAllUncached<T>(ModelTransaction transaction) where T : Model, new()
        {
            return await _globalDatastore.LoadAllUncached<T>(await GetDatastoreNamespace(), transaction);
        }

        public async Task Create<T>(T model, ModelTransaction transaction) where T : Model, new()
        {
            await _globalDatastore.Create<T>(await GetDatastoreNamespace(), model, transaction);
        }

        public async Task<T[]> CreateMany<T>(IList<T> models) where T : Model, new()
        {
            return await _globalDatastore.CreateMany<T>(await GetDatastoreNamespace(), models);
        }

        public async Task Upsert<T>(T model, ModelTransaction transaction) where T : Model, new()
        {
            await _globalDatastore.Upsert<T>(await GetDatastoreNamespace(), model, transaction);
        }

        public async Task Update<T>(T model, ModelTransaction transaction) where T : Model, new()
        {
            await _globalDatastore.Update<T>(await GetDatastoreNamespace(), model, transaction);
        }

        public async Task Delete<T>(T model, ModelTransaction transaction) where T : Model, new()
        {
            await _globalDatastore.Delete<T>(await GetDatastoreNamespace(), model, transaction);
        }

        public async Task<Key> AllocateKey<T>() where T : Model, new()
        {
            return await _globalDatastore.AllocateKey<T>(await GetDatastoreNamespace());
        }

        public async Task<ModelTransaction> BeginTransaction()
        {
            return await _globalDatastore.BeginTransaction(await GetDatastoreNamespace());
        }

        public async Task Commit(ModelTransaction transaction)
        {
            await _globalDatastore.Commit(await GetDatastoreNamespace(), transaction);
        }

        public async Task Rollback(ModelTransaction transaction)
        {
            await _globalDatastore.Rollback(await GetDatastoreNamespace(), transaction);
        }
    }
}
