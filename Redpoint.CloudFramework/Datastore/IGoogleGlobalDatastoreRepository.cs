﻿using Google.Cloud.Datastore.V1;
using Google.Type;
using Redpoint.CloudFramework.Geographic;
using Redpoint.CloudFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Datastore
{
    public interface IGoogleGlobalDatastoreRepository
    {
        DatastoreDb GetDatastoreDbForNamespace(string @namespace);

        Task<ModelQuery<T>> CreateQuery<T>(string @namespace) where T : Model, new();

        Task<MappedDatastoreQueryResults<T>> RunUncachedQuery<T>(string @namespace, ModelQuery<T> query,
            ReadOptions.Types.ReadConsistency readConsistency, ModelTransaction transaction = null) where T : Model, new();

        Task<MappedDatastoreQueryResults<T>> RunUncachedGeoQuery<T>(string @namespace, Expression<Func<T, LatLng>> fieldExpr, IGeoQuery geoquery, Filter[] additionalFilters = null) where T : Model, IGeoModel, new();

        Task<KeyFactory> GetKeyFactory(string @namespace, string kind);

        Task<KeyFactory> GetKeyFactory<T>(string @namespace) where T : Model, new();

        Task<Dictionary<Key, T>> LoadMany<T>(string @namespace, Key[] keys, ModelTransaction transaction = null) where T : Model, new();

        Task<Dictionary<Key, T>> LoadManyAcrossNamespaces<T>(Key[] keys) where T : Model, new();

        Task<T> LoadOneBy<T>(string @namespace, Key key, ModelTransaction transaction = null) where T : Model, new();

        Task<T> LoadOneBy<T, U>(string @namespace, string field, U value, ModelTransaction transaction = null) where T : Model, new();

        Task<List<T>> LoadAllBy<T, U>(string @namespace, string field, U value, ModelTransaction transaction = null) where T : Model, new();

        Task<List<T>> LoadAll<T>(string @namespace, ModelTransaction transaction = null) where T : Model, new();

        Task<List<T>> LoadAllUncached<T>(string @namespace, ModelTransaction transaction = null) where T : Model, new();

        Task Create<T>(string @namespace, T model, ModelTransaction transaction = null) where T : Model, new();

        Task<T[]> CreateMany<T>(string @namespace, IList<T> models) where T : Model, new();

        Task Upsert<T>(string @namespace, T model, ModelTransaction transaction = null) where T : Model, new();

        Task Update<T>(string @namespace, T model, ModelTransaction transaction = null) where T : Model, new();

        Task UpdateMany<T>(string @namespace, IList<T> models) where T : Model, new();

        Task Delete<T>(string @namespace, T model, ModelTransaction transaction = null) where T : Model, new();

        Task<Key> CreateNamedKey<T>(string @namespace, string name) where T : Model, new();

        Task<Key> AllocateKey<T>(string @namespace) where T : Model, new();

        Task<ModelTransaction> BeginTransaction(string @namespace);

        Task Commit(string @namespace, ModelTransaction transaction);

        Task Rollback(string @namespace, ModelTransaction transaction);
    }
}
