﻿using Google.Cloud.Datastore.V1;
using Google.Protobuf.WellKnownTypes;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Grpc.Core;
using Value = Google.Cloud.Datastore.V1.Value;
using NodaTime;
using Google.Cloud.Diagnostics.Common;
using Redpoint.CloudFramework.Prefix;
using Redpoint.CloudFramework.Error;
using Redpoint.CloudFramework.Models;
using Redpoint.CloudFramework.Event;
using Google.Type;
using Microsoft.Extensions.Logging;
using System.Collections.Concurrent;
using System.Threading;
using Redpoint.CloudFramework.Metric;
using Redpoint.CloudFramework.Geographic;
using System.Linq.Expressions;
using System.Globalization;
using Google.Protobuf;

namespace Redpoint.CloudFramework.Datastore
{
    public class GoogleGlobalDatastoreRepository : IGoogleGlobalDatastoreRepository
    {
        private readonly IDistributedCache _distributedCache;
        private readonly IGlobalPrefix _globalPrefix;
        private readonly DatastoreClient _client;
        private readonly IManagedTracer _managedTracer;
        private readonly ICommonExceptionFactory _commonExceptionFactory;
        private readonly ICurrentEnvironment _currentEnvironment;
        private readonly IEventApi _eventApi;
        private readonly IGoogleApiRetry _googleApiRetry;
        private readonly ILogger<GoogleGlobalDatastoreRepository> _logger;
        private readonly IMetricService _metricService;
        private readonly IGoogleGlobalDatastoreRepositoryHook[] _hooks;
        private readonly IGoogleGlobalDatastoreModelSerialization _googleGlobalDatastoreModelSerialization;
        private readonly IGoogleGlobalDatastoreModelMigration _googleGlobalDatastoreModelMigration;
        private readonly IInstantTimestampConversion _instantTimestampConversion;

        private const string DatastoreEntityReadMetric = "rcf/datastore_entity_read";
        private const string DatastoreEntityWriteMetric = "rcf/datastore_entity_write";
        private const string DatastoreEntityDeleteMetric = "rcf/datastore_entity_delete";
        private const string DatastoreSmallOpMetric = "rcf/datastore_small_op";

        private DistributedCacheEntryOptions _distributedCacheOptions =
            new DistributedCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(30))
                .SetAbsoluteExpiration(TimeSpan.FromHours(12));

        private MemoryCacheEntryOptions _memoryCacheOptions =
            new MemoryCacheEntryOptions()
                .SetSlidingExpiration(TimeSpan.FromMinutes(30))
                .SetAbsoluteExpiration(TimeSpan.FromHours(12));

        private IMemoryCache _memoryCache;

        private const string GeoHashPropertySuffix = ":geohash";
        private const string HashKeyPropertySuffix = ":hashkey";

        public GoogleGlobalDatastoreRepository(
            IDistributedCache distributedCache,
            IMemoryCache memoryCache,
            IGlobalPrefix globalPrefix,
            IManagedTracer managedTracer,
            ICommonExceptionFactory commonExceptionFactory,
            ICurrentEnvironment currentEnvironment,
            IEventApi eventApi,
            IGoogleApiRetry googleApiRetry,
            ILogger<GoogleGlobalDatastoreRepository> logger,
            IMetricService metricService,
            IGoogleGlobalDatastoreRepositoryHook[] hooks,
            IGoogleGlobalDatastoreModelSerialization googleGlobalDatastoreModelSerialization,
            IGoogleGlobalDatastoreModelMigration googleGlobalDatastoreModelMigration,
            IInstantTimestampConversion instantTimestampConversion)
        {
            _distributedCache = distributedCache;
            _memoryCache = memoryCache;
            _globalPrefix = globalPrefix;
            _managedTracer = managedTracer;
            _commonExceptionFactory = commonExceptionFactory;
            _currentEnvironment = currentEnvironment;
            _eventApi = eventApi;
            _googleApiRetry = googleApiRetry;
            _logger = logger;
            _metricService = metricService;
            _hooks = hooks;
            _googleGlobalDatastoreModelSerialization = googleGlobalDatastoreModelSerialization;
            _googleGlobalDatastoreModelMigration = googleGlobalDatastoreModelMigration;
            _instantTimestampConversion = instantTimestampConversion;

            _client = GoogleBuilder.Build<DatastoreClient, DatastoreClientBuilder>(
                _currentEnvironment,
                DatastoreClient.DefaultEndpoint,
                DatastoreClient.DefaultScopes);
        }

        private DatastoreDb GetDatastoreForCurrentSite(string @namespace)
        {
            using (_managedTracer.StartSpan("Db:GetDatastoreForCurrentSite(" + @namespace + ")"))
            {
                var db = _memoryCache.Get<DatastoreDb>("db:" + @namespace);
                if (db != null)
                {
                    return db;
                }

                db = DatastoreDb.Create(_currentEnvironment.GetGoogleProjectId(), @namespace, _client);
                _memoryCache.Set<DatastoreDb>("db:" + @namespace, db, _memoryCacheOptions);
                return db;
            }
        }

        public DatastoreDb GetDatastoreDbForNamespace(string @namespace)
        {
            return GetDatastoreForCurrentSite(@namespace);
        }

        public async Task<ModelQuery<T>> CreateQuery<T>(string @namespace) where T : Model, new()
        {
            return new ModelQuery<T>(@namespace, new Query(new T().GetKind()));
        }

        public async Task<MappedDatastoreQueryResults<T>> RunUncachedQuery<T>(string @namespace, ModelQuery<T> query, ReadOptions.Types.ReadConsistency readConsistency, ModelTransaction transaction) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:RunUncachedQuery<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    if (@namespace != query.Namespace)
                    {
                        throw new InvalidOperationException("Datastore namespaces do not match between creation and execution of query!");
                    }

                    var db = GetDatastoreForCurrentSite(@namespace);
                    DatastoreQueryResults rawResults;
                    if (transaction == null)
                    {
                        rawResults = db.RunQuery(query.Query, readConsistency);
                    }
                    else
                    {
                        rawResults = transaction.Transaction.RunQuery(query.Query);
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, rawResults.Entities.Count + 1, null, new Dictionary<string, string>
                    {
                        { "kind", (new T()).GetKind() },
                        { "ns", @namespace },
                    });

                    var results = new MappedDatastoreQueryResults<T>();
                    results.EndCursor = rawResults.EndCursor;
                    results.EndCursorForClients = rawResults.EndCursor.ToBase64();
                    results.MoreResults = rawResults.MoreResults;
                    results.Entities = new List<T>();

                    foreach (var rawEntity in rawResults.Entities)
                    {
                        results.Entities.Add(FromEntity<T>(@namespace, rawEntity));
                    }

                    // Workaround for Datastore Emulator bug - if we have no results, but the 
                    // status is MoreResultsAfterLimit, then really it should be NoMoreResults
                    // (otherwise we should have had some returned). Use the right status
                    // in this scenario.
                    if (results.Entities.Count == 0 && results.MoreResults == QueryResultBatch.Types.MoreResultsType.MoreResultsAfterLimit)
                    {
                        results.MoreResults = QueryResultBatch.Types.MoreResultsType.NoMoreResults;
                    }

                    return results;
                }
            });
        }

        public async Task<MappedDatastoreQueryResults<T>> RunUncachedGeoQuery<T>(string @namespace, Expression<Func<T, LatLng>> fieldExpr, IGeoQuery geoquery, Filter[] additionalFilters = null) where T : Model, IGeoModel, new()
        {
            var model = new T();

            var fieldExprLambda = fieldExpr.Compile();
            var fieldName = GetFieldNameFromExpression(fieldExpr);
            var keyLength = model.GetHashKeyLengthsForGeopointFields()[fieldName];

            var minPoint = geoquery.MinPoint;
            var maxPoint = geoquery.MaxPoint;
            var latLngRect = S2Manager.LatLngRectFromQueryRectangleInput(minPoint, maxPoint);
            var ranges = S2Manager.GetGeohashRanges(latLngRect, keyLength);

            var db = GetDatastoreForCurrentSite(@namespace);

            var tasks = new List<Task>();
            var rawResultsBag = new ConcurrentBag<Entity>();

            foreach (var range in ranges)
            {
                Func<ByteString, S2Manager.GeohashRange, Task> nextQuery = null;
                nextQuery = async (ByteString cursor, S2Manager.GeohashRange r) =>
                {
                    var hashKey = S2Manager.GenerateGeohashKey(r.RangeMin, keyLength);
                    var hashKeyString = hashKey.ToString(CultureInfo.InvariantCulture);

                    var filters = new List<Filter>
                    {
                        Filter.GreaterThan(fieldName + GeoHashPropertySuffix, new Value { StringValue = r.RangeMin.ToString(CultureInfo.InvariantCulture) }),
                        Filter.LessThan(fieldName + GeoHashPropertySuffix, new Value { StringValue = r.RangeMax.ToString(CultureInfo.InvariantCulture) }),
                        Filter.Equal(fieldName + HashKeyPropertySuffix, new Value { IntegerValue = (long)hashKey }),
                    };
                    if (additionalFilters != null)
                    {
                        filters.AddRange(additionalFilters);
                    }

                    var query = new Query(model.GetKind())
                    {
                        Filter = Filter.And(filters),
                        StartCursor = cursor,
                    };

                    DatastoreQueryResults rawResults = await db.RunQueryAsync(query, ReadOptions.Types.ReadConsistency.Eventual);
                    foreach (var rawEntity in rawResults.Entities)
                    {
                        rawResultsBag.Add(rawEntity);
                    }

                    // Workaround for Datastore Emulator bug - if we have no results, but the 
                    // status is MoreResultsAfterLimit, then really it should be NoMoreResults
                    // (otherwise we should have had some returned). Use the right status
                    // in this scenario.
                    var moreResultsState = rawResults.MoreResults;
                    if (rawResults.Entities.Count == 0 && rawResults.MoreResults == QueryResultBatch.Types.MoreResultsType.MoreResultsAfterLimit)
                    {
                        moreResultsState = QueryResultBatch.Types.MoreResultsType.NoMoreResults;
                    }

                    if (moreResultsState != QueryResultBatch.Types.MoreResultsType.NoMoreResults)
                    {
                        await nextQuery(rawResults.EndCursor, r);
                    }
                };

                tasks.Add(nextQuery(ByteString.Empty, range));
            }

            await Task.WhenAll(tasks);

            var rawResultsArray = rawResultsBag.ToArray();

            await _metricService.AddPoint(DatastoreEntityReadMetric, rawResultsArray.Length + 1, null, new Dictionary<string, string>
            {
                { "kind", (new T()).GetKind() },
                { "ns", @namespace },
            });

            var results = new MappedDatastoreQueryResults<T>();
            results.EndCursor = ByteString.Empty;
            results.EndCursorForClients = ByteString.Empty.ToBase64();
            results.MoreResults = QueryResultBatch.Types.MoreResultsType.NoMoreResults;

            var prefilterEntities = new List<T>();
            foreach (var rawEntity in rawResultsArray)
            {
                prefilterEntities.Add(FromEntity<T>(@namespace, rawEntity));
            }

            results.Entities = geoquery.PostQueryFilter(prefilterEntities, fieldExprLambda).ToList();

            return results;
        }

        private static string GetFieldNameFromExpression<T, T2>(Expression<Func<T, T2>> exp)
        {
            MemberExpression body = exp.Body as MemberExpression;

            if (body == null)
            {
                UnaryExpression ubody = (UnaryExpression)exp.Body;
                body = ubody.Operand as MemberExpression;
            }

            return body.Member.Name;
        }

        public async Task<KeyFactory> GetKeyFactory(string @namespace, string kind)
        {
            return new KeyFactory(_currentEnvironment.GetGoogleProjectId(), @namespace, kind);
        }

        public async Task<KeyFactory> GetKeyFactory<T>(string @namespace) where T : Model, new()
        {
            return new KeyFactory(_currentEnvironment.GetGoogleProjectId(), @namespace, new T().GetKind());
        }

        public async Task<Dictionary<Key, T>> LoadMany<T>(string @namespace, Key[] keys, ModelTransaction transaction = null) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:LoadMany<" + typeof(T).Name + ">(" + @namespace + ", ...)"))
                {
                    var @ref = new T();

                    var keysToLookup = keys.ToList();
                    var semaphore = new SemaphoreSlim(1);
                    var resolvedKeys = new ConcurrentDictionary<Key, T>();

                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("DbCache:GetString<" + typeof(T).Name + ">()"))
                        {
                            await Task.WhenAll(keys.Select(key => Task.Run(async () =>
                            {
                                var cachedValue = await _distributedCache.GetStringAsync(@namespace + ":" + @ref.GetKind() + ":KEY:" + _globalPrefix.CreateInternal(key));
                                if (cachedValue != null)
                                {
                                    try
                                    {
                                        if (resolvedKeys.TryAdd(key, FromJsonCache<T>(@namespace, cachedValue)))
                                        {
                                            await semaphore.WaitAsync();
                                            try
                                            {
                                                // We no longer need to query for this key because we loaded it's value from the cache.
                                                keysToLookup.Remove(key);
                                            }
                                            finally
                                            {
                                                semaphore.Release();
                                            }
                                        }
                                    }
                                    catch (CacheInvalidException)
                                    {
                                        // Continue to Datastore loading logic.
                                    }
                                }
                            })));
                        }
                    }

                    var factory = new KeyFactory(_currentEnvironment.GetGoogleProjectId(), @namespace, @ref.GetKind());

                    if (keysToLookup.Any(x => x == null))
                    {
                        throw new InvalidOperationException("One or more keys are null when passed to LoadMany");
                    }

                    IReadOnlyList<Entity> entities;
                    using (_managedTracer.StartSpan("Datastore:Lookup<" + typeof(T).Name + ">(" + @namespace + ", ...)"))
                    {
                        if (transaction == null)
                        {
                            entities = await GetDatastoreForCurrentSite(@namespace).LookupAsync(keysToLookup.ToArray());
                        }
                        else
                        {
                            entities = await transaction.Transaction.LookupAsync(keysToLookup.ToArray());
                        }
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, entities.Count, null, new Dictionary<string, string>
                    {
                        { "kind", @ref.GetKind() },
                        { "ns", @namespace },
                    });

                    var models = entities.Where(x => x != null).Select(entity => FromEntity<T>(@namespace, entity)).ToDictionary(k => k.Key, v => v);

                    using (_managedTracer.StartSpan("DbCache:SetString<" + typeof(T).Name + ">()"))
                    {
                        await Task.WhenAll(keysToLookup.Select(key => Task.Run(async () =>
                        {
                            var model = models.ContainsKey(key) ? models[key] : null;
                            await _distributedCache.SetStringAsync(@namespace + ":" + @ref.GetKind() + ":KEY:" + _globalPrefix.CreateInternal(key), ToJsonCache(model), _distributedCacheOptions);
                        })));
                    }

                    foreach (var model in models)
                    {
                        resolvedKeys.TryAdd(model.Key, model.Value);
                    }

                    return resolvedKeys.ToDictionary(k => k.Key, v => v.Value);
                }
            });
        }

        public async Task<Dictionary<Key, T>> LoadManyAcrossNamespaces<T>(Key[] keys) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:LoadManyAcrossNamespaces<" + typeof(T).Name + ">(...)"))
                {
                    var @ref = new T();

                    var keysToLookup = keys.ToList();
                    var semaphore = new SemaphoreSlim(1);
                    var resolvedKeys = new ConcurrentDictionary<Key, T>();

                    using (_managedTracer.StartSpan("DbCache:GetString<" + typeof(T).Name + ">()"))
                    {
                        await Task.WhenAll(keys.Select(key => Task.Run(async () =>
                        {
                            var cachedValue = await _distributedCache.GetStringAsync(key.PartitionId.NamespaceId + ":" + @ref.GetKind() + ":KEY:" + _globalPrefix.CreateInternal(key));
                            if (cachedValue != null)
                            {
                                try
                                {
                                    if (resolvedKeys.TryAdd(key, FromJsonCache<T>(key.PartitionId.NamespaceId, cachedValue)))
                                    {
                                        await semaphore.WaitAsync();
                                        try
                                        {
                                            // We no longer need to query for this key because we loaded it's value from the cache.
                                            keysToLookup.Remove(key);
                                        }
                                        finally
                                        {
                                            semaphore.Release();
                                        }
                                    }
                                }
                                catch (CacheInvalidException)
                                {
                                    // Continue to Datastore loading logic.
                                }
                            }
                        })));
                    }

                    if (keysToLookup.Any(x => x == null))
                    {
                        throw new InvalidOperationException("One or more keys are null when passed to LoadMany");
                    }

                    var entities = new List<Entity>();
                    foreach (var keysToLookupGrouped in keysToLookup.GroupBy(x => x.PartitionId.NamespaceId))
                    {
                        using (_managedTracer.StartSpan("Datastore:Lookup<" + typeof(T).Name + ">(...)"))
                        {
                            entities.AddRange(await GetDatastoreForCurrentSite(keysToLookupGrouped.Key).LookupAsync(keysToLookupGrouped.ToArray()));
                        }
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, entities.Count, null, new Dictionary<string, string>
                    {
                        { "kind", @ref.GetKind() },
                    });

                    var models = entities.Where(x => x != null).Select(entity => FromEntity<T>(entity.Key.PartitionId.NamespaceId, entity)).ToDictionary(k => k.Key, v => v);

                    using (_managedTracer.StartSpan("DbCache:SetString<" + typeof(T).Name + ">()"))
                    {
                        await Task.WhenAll(keysToLookup.Select(key => Task.Run(async () =>
                        {
                            var model = models.ContainsKey(key) ? models[key] : null;
                            await _distributedCache.SetStringAsync(key.PartitionId.NamespaceId + ":" + @ref.GetKind() + ":KEY:" + _globalPrefix.CreateInternal(key), ToJsonCache(model), _distributedCacheOptions);
                        })));
                    }

                    foreach (var model in models)
                    {
                        resolvedKeys.TryAdd(model.Key, model.Value);
                    }

                    return resolvedKeys.ToDictionary(k => k.Key, v => v.Value);
                }
            });
        }

        public async Task<T> LoadOneBy<T>(string @namespace, Key key, ModelTransaction transaction) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:LoadOneBy<" + typeof(T).Name + ">(" + @namespace + ", " + _globalPrefix.CreateInternal(key) + ")"))
                {
                    var @ref = new T();

                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("DbCache:GetString<" + typeof(T).Name + ">()"))
                        {
                            var cachedValue = await _distributedCache.GetStringAsync(@namespace + ":" + @ref.GetKind() + ":KEY:" + _globalPrefix.CreateInternal(key));
                            if (cachedValue != null)
                            {
                                try
                                {
                                    return FromJsonCache<T>(@namespace, cachedValue);
                                }
                                catch (CacheInvalidException)
                                {
                                    // Continue to Datastore loading logic.
                                }
                            }
                        }
                    }

                    var factory = new KeyFactory(_currentEnvironment.GetGoogleProjectId(), @namespace, @ref.GetKind());

                    Entity entity;
                    using (_managedTracer.StartSpan("Datastore:Lookup<" + typeof(T).Name + ">(" + @namespace + ", " + _globalPrefix.CreateInternal(key) + ")"))
                    {
                        if (transaction == null)
                        {
                            entity = await GetDatastoreForCurrentSite(@namespace).LookupAsync(key);
                        }
                        else
                        {
                            entity = await transaction.Transaction.LookupAsync(key);
                        }
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, 1, null, new Dictionary<string, string>
                    {
                        { "kind", @ref.GetKind() },
                        { "ns", @namespace },
                    });

                    var model = entity == null ? null : FromEntity<T>(@namespace, entity);

                    using (_managedTracer.StartSpan("DbCache:SetString<" + typeof(T).Name + ">()"))
                    {
                        await _distributedCache.SetStringAsync(@namespace + ":" + @ref.GetKind() + ":KEY:" + _globalPrefix.CreateInternal(key), ToJsonCache(model), _distributedCacheOptions);
                    }

                    return model;
                }
            });
        }

        private (string serialized, Value datastore) GetValuesForIndexCache<U>(U value)
        {
            if (value == null)
            {
                return ("<null>", new Value { NullValue = NullValue.NullValue });
            }

            switch (value)
            {
                case bool b:
                    return (b.ToString(), new Value { BooleanValue = b });
                case string s:
                    return (s, new Value { StringValue = s });
                case long l:
                    return (l.ToString(), new Value { IntegerValue = l });
                case Key k:
                    return (_globalPrefix.CreateInternal(k), new Value { KeyValue = k });
            }

            throw new NotSupportedException("Invalid value for serialized index cache: " + value);
        }

        public async Task<T> LoadOneBy<T, U>(string @namespace, string field, U value, ModelTransaction transaction) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                var (serializedIndexValue, indexValue) = GetValuesForIndexCache(value);

                using (_managedTracer.StartSpan("Db:LoadOneBy<" + typeof(T).Name + ">(" + @namespace + ", " + field + "=" + serializedIndexValue + ")"))
                {
                    if (field == null)
                    {
                        throw new ArgumentNullException(nameof(field));
                    }

                    var @ref = new T();

                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("DbCache:GetString<" + typeof(T).Name + ">()"))
                        {
                            var cachedValue = await _distributedCache.GetStringAsync(@namespace + ":" + @ref.GetKind() + ":ONE:" + field + "=" + serializedIndexValue);
                            if (cachedValue != null)
                            {
                                try
                                {
                                    return FromJsonCache<T>(@namespace, cachedValue);
                                }
                                catch (CacheInvalidException)
                                {
                                    // Continue to Datastore loading logic.
                                }
                            }
                        }
                    }

                    Entity entity = null;
                    using (_managedTracer.StartSpan("Datastore:Query<" + typeof(T).Name + ">(" + @namespace + ", " + field + "=" + serializedIndexValue + ")"))
                    {
                        var query = new Query(@ref.GetKind())
                        {
                            Filter = Filter.Equal(field, indexValue),
                            Limit = 1
                        };

                        var db = GetDatastoreForCurrentSite(@namespace);
                        AsyncLazyDatastoreQuery result;
                        if (transaction == null)
                        {
                            result = db.RunQueryLazilyAsync(query, ReadOptions.Types.ReadConsistency.Eventual);
                        }
                        else
                        {
                            result = transaction.Transaction.RunQueryLazilyAsync(query);
                        }
                        await foreach (var value in result)
                        {
                            entity = value;
                            break;
                        }
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, entity == null ? 1 : 2, null, new Dictionary<string, string>
                    {
                        { "kind", @ref.GetKind() },
                        { "ns", @namespace },
                    });

                    var model = entity == null ? null : FromEntity<T>(@namespace, entity);

                    using (_managedTracer.StartSpan("DbCache:SetString<" + typeof(T).Name + ">()"))
                    {
                        await _distributedCache.SetStringAsync(@namespace + ":" + @ref.GetKind() + ":ONE:" + field + "=" + serializedIndexValue, ToJsonCache(model), _distributedCacheOptions);
                    }

                    return model;
                }
            });
        }

        public async Task<List<T>> LoadAllBy<T, U>(string @namespace, string field, U value, ModelTransaction transaction) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                var (serializedIndexValue, indexValue) = GetValuesForIndexCache(value);

                using (_managedTracer.StartSpan("Db:LoadAllBy<" + typeof(T).Name + ">(" + @namespace + ", " + field + "=" + serializedIndexValue + ")"))
                {
                    if (field == null)
                    {
                        throw new ArgumentNullException(nameof(field));
                    }

                    var @ref = new T();

                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("DbCache:GetString<" + typeof(T).Name + ">()"))
                        {
                            var cachedValue = await _distributedCache.GetStringAsync(@namespace + ":" + @ref.GetKind() + ":ALL:" + field + "=" + serializedIndexValue);
                            if (cachedValue != null)
                            {
                                var listOfJsonObjects = JsonConvert.DeserializeObject<string[]>(cachedValue);
                                var cachedResults = listOfJsonObjects.Select(x =>
                                {
                                    try
                                    {
                                        return FromJsonCache<T>(@namespace, x);
                                    }
                                    catch (CacheInvalidException)
                                    {
                                        return null;
                                    }
                                }).ToList();
                                if (cachedResults.All(x => x != null))
                                {
                                    return cachedResults;
                                }
                            }
                        }
                    }

                    List<Entity> entities = new List<Entity>();
                    var results = new List<T>();
                    using (_managedTracer.StartSpan("Datastore:Query<" + typeof(T).Name + ">(" + @namespace + ", " + field + "=" + serializedIndexValue + ")"))
                    {
                        var query = new Query(@ref.GetKind())
                        {
                            Filter = Filter.Equal(field, indexValue)
                        };

                        var db = GetDatastoreForCurrentSite(@namespace);
                        AsyncLazyDatastoreQuery result;
                        if (transaction == null)
                        {
                            result = db.RunQueryLazilyAsync(query, ReadOptions.Types.ReadConsistency.Eventual);
                        }
                        else
                        {
                            result = transaction.Transaction.RunQueryLazilyAsync(query);
                        }
                        await foreach (var value in result)
                        {
                            entities.Add(value);
                        }
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, entities.Count + 1, null, new Dictionary<string, string>
                    {
                        { "kind", @ref.GetKind() },
                        { "ns", @namespace },
                    });

                    foreach (var entity in entities)
                    {
                        var model = FromEntity<T>(@namespace, entity);

                        results.Add(model);
                    }

                    using (_managedTracer.StartSpan("DbCache:SetString<" + typeof(T).Name + ">()"))
                    {
                        var listOfJsonObjectsToCache = results.Select(x => ToJsonCache(x)).ToArray();
                        await _distributedCache.SetStringAsync(
                            @namespace + ":" + @ref.GetKind() + ":ALL:" + field + "=" + serializedIndexValue,
                            JsonConvert.SerializeObject(listOfJsonObjectsToCache),
                            _distributedCacheOptions);
                    }

                    return results;
                }
            });
        }

        public async Task<List<T>> LoadAll<T>(string @namespace, ModelTransaction transaction) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:LoadAll<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    var @ref = new T();

                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("DbCache:GetString<" + typeof(T).Name + ">()"))
                        {
                            var cachedValue = await _distributedCache.GetStringAsync(@namespace + ":" + @ref.GetKind() + ":ALL");
                            if (cachedValue != null)
                            {
                                var listOfJsonObjects = JsonConvert.DeserializeObject<string[]>(cachedValue);
                                var cachedResults = listOfJsonObjects.Select(x =>
                                {
                                    try
                                    {
                                        return FromJsonCache<T>(@namespace, x);
                                    }
                                    catch (CacheInvalidException)
                                    {
                                        return null;
                                    }
                                }).ToList();
                                if (cachedResults.All(x => x != null))
                                {
                                    return cachedResults;
                                }
                            }
                        }
                    }

                    List<Entity> entities = new List<Entity>();
                    var results = new List<T>();
                    using (_managedTracer.StartSpan("Datastore:Query<" + typeof(T).Name + ">(" + @namespace + ")"))
                    {
                        var query = new Query(@ref.GetKind());

                        var db = GetDatastoreForCurrentSite(@namespace);
                        AsyncLazyDatastoreQuery result;
                        if (transaction == null)
                        {
                            result = db.RunQueryLazilyAsync(query, ReadOptions.Types.ReadConsistency.Eventual);
                        }
                        else
                        {
                            result = transaction.Transaction.RunQueryLazilyAsync(query);
                        }
                        await foreach (var value in result)
                        {
                            entities.Add(value);
                        }
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, entities.Count + 1, null, new Dictionary<string, string>
                    {
                        { "kind", @ref.GetKind() },
                        { "ns", @namespace },
                    });

                    foreach (var entity in entities)
                    {
                        var model = FromEntity<T>(@namespace, entity);

                        results.Add(model);
                    }

                    using (_managedTracer.StartSpan("DbCache:SetString<" + typeof(T).Name + ">()"))
                    {
                        var listOfJsonObjectsToCache = results.Select(x => ToJsonCache(x)).ToArray();
                        await _distributedCache.SetStringAsync(
                            @namespace + ":" + @ref.GetKind() + ":ALL",
                            JsonConvert.SerializeObject(listOfJsonObjectsToCache),
                            _distributedCacheOptions);
                    }

                    return results;
                }
            });
        }

        public async Task<List<T>> LoadAllUncached<T>(string @namespace, ModelTransaction transaction) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:LoadAllUncached<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    var @ref = new T();

                    List<Entity> entities = new List<Entity>();
                    var results = new List<T>();
                    using (_managedTracer.StartSpan("Datastore:Query<" + typeof(T).Name + ">(" + @namespace + ")"))
                    {
                        var query = new Query(@ref.GetKind());

                        var db = GetDatastoreForCurrentSite(@namespace);
                        AsyncLazyDatastoreQuery result;
                        if (transaction == null)
                        {
                            result = db.RunQueryLazilyAsync(query, ReadOptions.Types.ReadConsistency.Eventual);
                        }
                        else
                        {
                            result = transaction.Transaction.RunQueryLazilyAsync(query);
                        }
                        await foreach (var value in result)
                        {
                            entities.Add(value);
                        }
                    }

                    await _metricService.AddPoint(DatastoreEntityReadMetric, entities.Count + 1, null, new Dictionary<string, string>
                    {
                        { "kind", @ref.GetKind() },
                        { "ns", @namespace },
                    });

                    foreach (var entity in entities)
                    {
                        var model = FromEntity<T>(@namespace, entity);

                        results.Add(model);
                    }

                    return results;
                }
            });
        }

        public async Task Create<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new()
        {
            await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:Create<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    var target = ToEntity(@namespace, model, true);

                    var db = GetDatastoreForCurrentSite(@namespace);
                    Key autoKey;
                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("Datastore:Insert<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            autoKey = await db.InsertAsync(target);

                            if (autoKey != null)
                            {
                                // For PostCreate hooks...
                                model.Key = autoKey;
                            }

                            foreach (var hook in _hooks)
                            {
                                await hook.PostCreate(@namespace, model, transaction);
                            }

                            await _metricService.AddPoint(DatastoreEntityWriteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                    }
                    else
                    {
                        if (target.Key.Path.Last().IdTypeCase == Key.Types.PathElement.IdTypeOneofCase.None)
                        {
                            using (_managedTracer.StartSpan("Datastore:AllocateId<" + typeof(T).Name + ">(" + @namespace + ")"))
                            {
                                // Allocate an ID outside of the transaction.
                                target.Key = await db.AllocateIdAsync(target.Key);

                                await _metricService.AddPoint(DatastoreSmallOpMetric, 1, null, new Dictionary<string, string>
                                {
                                    { "kind", model.GetKind() },
                                    { "ns", @namespace },
                                });
                            }
                        }
                        using (_managedTracer.StartSpan("Datastore:Transaction:Insert<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            transaction.Transaction.Insert(target);
                            transaction.ModifiedModels.Add(model);
                            transaction.QueuedPreCommitOperations.Add(async () =>
                            {
                                foreach (var hook in _hooks)
                                {
                                    await hook.PostCreate(@namespace, model, transaction);
                                }
                            });

                            await _metricService.AddPoint(DatastoreEntityWriteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                        autoKey = target.Key;
                    }
                    if (autoKey != null)
                    {
                        model.Key = autoKey;
                    }

                    await ClearCaches(@namespace, model);
                }
            });
        }

        public async Task<T[]> CreateMany<T>(string @namespace, IList<T> modelsAll) where T : Model, new()
        {
            var @ref = new T();

            var result = new List<T>();
            for (var idx = 0; idx < modelsAll.Count; idx += 25)
            {
                var models = new List<T>();
                for (var a = idx; a < Math.Min(modelsAll.Count, idx + 25); a++)
                {
                    models.Add(modelsAll[a]);
                }

                result.AddRange(await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.DatastoreTransactional, _logger, async () =>
                {
                    using (_managedTracer.StartSpan("Db:CreateMany<" + typeof(T).Name + ">(" + @namespace + ")"))
                    {
                        var results = new List<T>();
                        var targets = models.ToDictionary(m => m, model => ToEntity(@namespace, model, true));

                        var db = GetDatastoreForCurrentSite(@namespace);
                        var transaction = await db.BeginTransactionAsync();
                        var didCommit = false;
                        try
                        {
                            var allKeys = targets.Values.Where(x => x.Key.Path.All(y => y.IdTypeCase != Key.Types.PathElement.IdTypeOneofCase.None)).Select(x => x.Key).ToArray();
                            var existingKeys = new HashSet<Key>((await transaction.LookupAsync(allKeys)).Where(x => x != null).Select(x => x.Key));

                            await _metricService.AddPoint(DatastoreEntityReadMetric, allKeys.Length, null, new Dictionary<string, string>
                            {
                                { "kind", @ref.GetKind() },
                                { "ns", @namespace },
                            });

                            var toInsert = new List<Entity>();
                            foreach (var target in targets)
                            {
                                if (target.Value.Key.Path.Last().IdTypeCase == Key.Types.PathElement.IdTypeOneofCase.None)
                                {
                                    target.Value.Key = await db.AllocateIdAsync(target.Value.Key);

                                    await _metricService.AddPoint(DatastoreSmallOpMetric, 1, null, new Dictionary<string, string>
                                    {
                                        { "kind", @ref.GetKind() },
                                        { "ns", @namespace },
                                    });
                                }

                                if (!existingKeys.Contains(target.Value.Key))
                                {
                                    toInsert.Add(target.Value);
                                    results.Add(target.Key);
                                }
                            }

                            foreach (var hook in _hooks)
                            {
                                foreach (var target in toInsert)
                                {
                                    await hook.MutateEntityBeforeWrite(@namespace, target);
                                }
                            }

                            transaction.Insert(toInsert);

                            await _metricService.AddPoint(DatastoreEntityWriteMetric, toInsert.Count, null, new Dictionary<string, string>
                            {
                                { "kind", @ref.GetKind() },
                                { "ns", @namespace },
                            });

                            foreach (var hook in _hooks)
                            {
                                foreach (var r in results)
                                {
                                    await hook.PostCreate(@namespace, r, null);
                                }
                            }

                            await transaction.CommitAsync();
                            didCommit = true;
                        }
                        finally
                        {
                            if (!didCommit)
                            {
                                await transaction.RollbackAsync();
                            }
                            else
                            {
                                foreach (var target in targets)
                                {
                                    // Copy any assignd keys.
                                    target.Key.Key = target.Value.Key;
                                }
                            }
                        }

                        var tasks = new Task[results.Count];
                        for (var i = 0; i < results.Count; i++)
                        {
                            tasks[i] = ClearCaches(@namespace, results[i]);
                        }
                        await Task.WhenAll(tasks);

                        return results.ToArray();
                    }
                }));
            }
            return result.ToArray();
        }

        public async Task Upsert<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new()
        {
            await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:Upsert<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    var target = ToEntity(@namespace, model, false);

                    var db = GetDatastoreForCurrentSite(@namespace);
                    Key autoKey;
                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("Datastore:Upsert<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            autoKey = await db.UpsertAsync(target);

                            if (autoKey != null)
                            {
                                // For PostUpsert hooks...
                                model.Key = autoKey;
                            }
                            foreach (var hook in _hooks)
                            {
                                await hook.PostUpsert(@namespace, model, transaction);
                            }

                            await _metricService.AddPoint(DatastoreEntityWriteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                    }
                    else
                    {
                        if (target.Key.Path.Last().IdTypeCase == Key.Types.PathElement.IdTypeOneofCase.None)
                        {
                            using (_managedTracer.StartSpan("Datastore:AllocateId<" + typeof(T).Name + ">(" + @namespace + ")"))
                            {
                                // Allocate an ID outside of the transaction.
                                target.Key = await db.AllocateIdAsync(target.Key);

                                await _metricService.AddPoint(DatastoreSmallOpMetric, 1, null, new Dictionary<string, string>
                                {
                                    { "kind", model.GetKind() },
                                    { "ns", @namespace },
                                });
                            }
                        }
                        using (_managedTracer.StartSpan("Datastore:Transaction:Upsert<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            transaction.Transaction.Upsert(target);
                            transaction.ModifiedModels.Add(model);
                            transaction.QueuedPreCommitOperations.Add(async () =>
                            {
                                foreach (var hook in _hooks)
                                {
                                    await hook.PostUpsert(@namespace, model, transaction);
                                }
                            });

                            await _metricService.AddPoint(DatastoreEntityWriteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                        autoKey = target.Key;
                    }
                    if (autoKey != null)
                    {
                        model.Key = autoKey;
                    }

                    await ClearCaches(@namespace, model);
                }
            });
        }

        public async Task Update<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new()
        {
            await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:Update<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    var target = ToEntity(@namespace, model, false);

                    var db = GetDatastoreForCurrentSite(@namespace);
                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("Datastore:Update<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            await db.UpdateAsync(target);

                            foreach (var hook in _hooks)
                            {
                                await hook.PostUpdate(@namespace, model, transaction);
                            }

                            await _metricService.AddPoint(DatastoreEntityWriteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                    }
                    else
                    {
                        using (_managedTracer.StartSpan("Datastore:Transaction:Update<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            transaction.Transaction.Update(target);
                            transaction.ModifiedModels.Add(model);
                            transaction.QueuedPreCommitOperations.Add(async () =>
                            {
                                foreach (var hook in _hooks)
                                {
                                    await hook.PostUpdate(@namespace, model, transaction);
                                }
                            });

                            await _metricService.AddPoint(DatastoreEntityWriteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                    }

                    await ClearCaches(@namespace, model);
                }
            });
        }

        public async Task UpdateMany<T>(string @namespace, IList<T> models) where T : Model, new()
        {
            var @ref = new T();

            await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.DatastoreTransactional, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:UpdateMany<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    if (models.Count > 500)
                    {
                        throw new InvalidOperationException("Number of models passed to UpdateMany exceeds 500");
                    }

                    var targets = models.Select(x => ToEntity(@namespace, x, false)).ToArray();

                    var db = GetDatastoreForCurrentSite(@namespace);
                    var transaction = await db.BeginTransactionAsync();
                    var didCommit = false;
                    try
                    {
                        foreach (var hook in _hooks)
                        {
                            foreach (var target in targets)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }
                        }

                        transaction.Update(targets);

                        await _metricService.AddPoint(DatastoreEntityWriteMetric, targets.Length, null, new Dictionary<string, string>
                        {
                            { "kind", @ref.GetKind() },
                            { "ns", @namespace },
                        });

                        foreach (var hook in _hooks)
                        {
                            foreach (var model in models)
                            {
                                await hook.PostUpsert(@namespace, model, null);
                            }
                        }

                        await transaction.CommitAsync();
                        didCommit = true;
                    }
                    finally
                    {
                        if (!didCommit)
                        {
                            await transaction.RollbackAsync();
                        }
                    }

                    var tasks = new Task[models.Count];
                    for (var i = 0; i < models.Count; i++)
                    {
                        tasks[i] = ClearCaches(@namespace, models[i]);
                    }
                    await Task.WhenAll(tasks);
                }
            });
        }

        public async Task Delete<T>(string @namespace, T model, ModelTransaction transaction) where T : Model, new()
        {
            await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:Delete<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    var target = ToEntity(@namespace, model, false);

                    var db = GetDatastoreForCurrentSite(@namespace);
                    if (transaction == null)
                    {
                        using (_managedTracer.StartSpan("Datastore:Delete<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            await db.DeleteAsync(target);

                            foreach (var hook in _hooks)
                            {
                                await hook.PostDelete(@namespace, model, transaction);
                            }

                            await _metricService.AddPoint(DatastoreEntityDeleteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                    }
                    else
                    {
                        using (_managedTracer.StartSpan("Datastore:Transaction:Delete<" + typeof(T).Name + ">(" + @namespace + ")"))
                        {
                            foreach (var hook in _hooks)
                            {
                                await hook.MutateEntityBeforeWrite(@namespace, target);
                            }

                            transaction.Transaction.Delete(target);
                            transaction.ModifiedModels.Add(model);
                            transaction.QueuedPreCommitOperations.Add(async () =>
                            {
                                foreach (var hook in _hooks)
                                {
                                    await hook.PostDelete(@namespace, model, transaction);
                                }
                            });

                            await _metricService.AddPoint(DatastoreEntityDeleteMetric, 1, null, new Dictionary<string, string>
                            {
                                { "kind", model.GetKind() },
                                { "ns", @namespace },
                            });
                        }
                    }

                    await ClearCaches(@namespace, model);
                }
            });
        }

        public async Task<Key> AllocateKey<T>(string @namespace) where T : Model, new()
        {
            return await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.Datastore, _logger, async () =>
            {
                using (_managedTracer.StartSpan("Db:AllocateKey<" + typeof(T).Name + ">(" + @namespace + ")"))
                {
                    var model = new T();
                    var db = GetDatastoreForCurrentSite(@namespace);
                    var factory = db.CreateKeyFactory(model.GetKind());
                    var key = factory.CreateIncompleteKey();
                    using (_managedTracer.StartSpan("Datastore:AllocateId<" + typeof(T).Name + ">(" + @namespace + ")"))
                    {
                        var result = await db.AllocateIdAsync(key);

                        await _metricService.AddPoint(DatastoreSmallOpMetric, 1, null, new Dictionary<string, string>
                        {
                            { "kind", model.GetKind() },
                            { "ns", @namespace },
                        });

                        return result;
                    }
                }
            });
        }

        public async Task<Key> CreateNamedKey<T>(string @namespace, string name) where T : Model, new()
        {
            var model = new T();
            var db = GetDatastoreForCurrentSite(@namespace);
            var factory = db.CreateKeyFactory(model.GetKind());
            return factory.CreateKey(name);
        }

        public async Task<ModelTransaction> BeginTransaction(string @namespace)
        {
            return new ModelTransaction(@namespace, await GetDatastoreDbForNamespace(@namespace).BeginTransactionAsync());
        }

        public async Task Commit(string @namespace, ModelTransaction transaction)
        {
            await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                foreach (var action in transaction.QueuedPreCommitOperations)
                {
                    await action();
                }

                await transaction.Transaction.CommitAsync();

                var tasks = new Task[transaction.ModifiedModels.Count];
                for (var i = 0; i < transaction.ModifiedModels.Count; i++)
                {
                    if (transaction.ModifiedModels[i].Key != null)
                    {
                        var localNamespace = transaction.ModifiedModels[i].Key.PartitionId.NamespaceId;
                        tasks[i] = ClearCaches(localNamespace, transaction.ModifiedModels[i]);
                    }
                    else
                    {
                        tasks[i] = ClearCaches(@namespace, transaction.ModifiedModels[i]);
                    }
                }
                await Task.WhenAll(tasks);
            });
        }

        public async Task Rollback(string @namespace, ModelTransaction transaction)
        {
            await _googleApiRetry.DoRetryableOperationAsync(transaction != null ? GoogleApiCallContext.DatastoreTransactional : GoogleApiCallContext.Datastore, _logger, async () =>
            {
                try
                {
                    await transaction.Transaction.RollbackAsync();
                }
                catch (RpcException ex) when (ex.IsTransactionExpiryException())
                {
                    // Rollback isn't needed, continue.
                }
            });
        }

        private async Task ClearCaches(string @namespace, Model model)
        {
            using (_managedTracer.StartSpan("DbCache:Clear<" + model.GetType().Name + ">()"))
            {
                var prefix = @namespace + ":" + model.GetKind();

                var cacheClearing = new List<Task>();
                cacheClearing.Add(_distributedCache.RemoveAsync(prefix + ":KEY:" + _globalPrefix.CreateInternal(model.Key)));
                cacheClearing.Add(_distributedCache.RemoveAsync(prefix + ":ALL"));

                foreach (var entry in model.GetIndexes())
                {
                    var typeInfo = model.GetType().GetTypeInfo();
                    var propInfo = typeInfo.GetDeclaredProperty(entry);
                    if (propInfo != null)
                    {
                        try
                        {
                            var value = propInfo.GetValue(model);
                            var (serializedIndexValue, _) = GetValuesForIndexCache(value);

                            cacheClearing.Add(_distributedCache.RemoveAsync(prefix + ":ONE:" + entry + "=" + serializedIndexValue));
                            cacheClearing.Add(_distributedCache.RemoveAsync(prefix + ":ALL:" + entry + "=" + serializedIndexValue));

                            if (model._originalData != null)
                            {
                                var oldValue = model._originalData[entry];
                                var (oldSerializedIndexValue, _) = GetValuesForIndexCache(oldValue);

                                if (oldSerializedIndexValue != serializedIndexValue)
                                {
                                    cacheClearing.Add(_distributedCache.RemoveAsync(prefix + ":ONE:" + entry + "=" + oldSerializedIndexValue));
                                    cacheClearing.Add(_distributedCache.RemoveAsync(prefix + ":ALL:" + entry + "=" + oldSerializedIndexValue));
                                }
                            }
                        }
                        catch
                        {
                        }
                    }
                }

                await Task.WhenAll(cacheClearing);
            }
        }

        private T FromEntity<T>(string @namespace, Entity entity) where T : Model, new()
        {
            using (_managedTracer.StartSpan("DbSer:FromEntity<" + typeof(T).Name + ">()"))
            {
                var @ref = new T();
                @ref._originalData = new Dictionary<string, object>();

                var delayedLocalKeyAssignments = new List<Action<string>>();

                var types = @ref.GetTypes();
                foreach (var kv in types)
                {
                    var typeInfo = @ref.GetType().GetTypeInfo();
                    var propInfo = typeInfo.GetDeclaredProperty(kv.Key);
                    if (propInfo == null)
                    {
                        _logger.LogWarning("Model {0} declares property {1} but is missing C# declaration", typeof(T).FullName, kv.Key);
                        continue;
                    }

                    object value = null;
                    if (entity[kv.Key]?.IsNull ?? true)
                    {
                        // Preserve null.
                    }
                    else
                    {
                        switch (kv.Value)
                        {
                            case "string":
                                value = entity[kv.Key]?.StringValue;
                                break;
                            case "boolean":
                                value = entity[kv.Key]?.BooleanValue;
                                break;
                            case "integer":
                                value = entity[kv.Key]?.IntegerValue;
                                break;
                            case "geopoint":
                                value = entity[kv.Key]?.GeoPointValue;
                                break;
                            case "key":
                                var keyValue = entity[kv.Key]?.KeyValue;

                                if (keyValue.PartitionId.NamespaceId != @namespace)
                                {
                                    throw new InvalidOperationException("Unable to load property '" + kv.Key + "' from entity; cross-namespace reference detected");
                                }

                                value = keyValue;
                                break;
                            case "local-key":
                                var localKeyValue = entity[kv.Key]?.KeyValue;

                                if (@namespace != string.Empty)
                                {
                                    throw new InvalidOperationException("local-key properties can not be used on entities outside the global namespace");
                                }

                                // We can't assign yet because we need to check that the loaded namespace value is 
                                // valid for GetDatastoreNamespaceForLocalKeys, but we can't use that method
                                // until everything else has been loaded.
                                delayedLocalKeyAssignments.Add((@localNamespace) =>
                                {
                                    if (localKeyValue.PartitionId.NamespaceId != localNamespace)
                                    {
                                        throw new InvalidOperationException("Unable to load property '" + kv.Key + "' from entity; cross-namespace reference detected");
                                    }

                                    propInfo.SetValue(@ref, localKeyValue);
                                    @ref._originalData[kv.Key] = localKeyValue;
                                });

                                value = null;
                                break;
                            case "global-key":
                                var globalKeyValue = entity[kv.Key]?.KeyValue;

                                if (@namespace == string.Empty)
                                {
                                    throw new InvalidOperationException("global-key properties can not be used on entities inside the global namespace");
                                }
                                if (globalKeyValue != null && globalKeyValue.PartitionId.NamespaceId != string.Empty)
                                {
                                    throw new InvalidOperationException("Unable to load property '" + kv.Key + "' from entity; cross-namespace reference detected");
                                }

                                value = globalKeyValue;
                                break;
                            case "unsafe-key":
                                value = entity[kv.Key]?.KeyValue;
                                break;
                            case "timestamp":
                                value = FromDatastoreValueToNodaTimeInstant(entity[kv.Key]);
                                break;
                            case "json":
                                {
                                    var rawJson = entity[kv.Key]?.StringValue;
                                    if (rawJson == null)
                                    {
                                        value = null;
                                    }
                                    else
                                    {
                                        value = JsonConvert.DeserializeObject(rawJson, propInfo.PropertyType, new VersionedJsonConverter(), new NodaTimeInstantJsonConverter());
                                    }
                                    break;
                                }
                            default:
                                throw new InvalidOperationException("Invalid type defined for property: " + kv.Value);
                        }
                    }

                    propInfo.SetValue(@ref, value);

                    @ref._originalData[kv.Key] = value;
                }

                @ref.dateCreatedUtc = FromDatastoreValueToNodaTimeInstant(entity["dateCreatedUtc"]);
                @ref.dateModifiedUtc = FromDatastoreValueToNodaTimeInstant(entity["dateModifiedUtc"]);
                @ref.Key = entity.Key;
                if (entity["schemaVersion"]?.IsNull ?? true || entity["schemaVersion"].ValueTypeCase != Value.ValueTypeOneofCase.IntegerValue)
                {
                    @ref.schemaVersion = null;
                }
                else
                {
                    @ref.schemaVersion = entity["schemaVersion"].IntegerValue;
                }
                @ref.hasImplicitMigrationsApplied = false;

                // If we have any delayed local key assignments, run them now (before migrations, in case
                // migrations want to handle local-key properties).
                if (delayedLocalKeyAssignments.Count > 0)
                {
                    var localNamespace = @ref.GetDatastoreNamespaceForLocalKeys();
                    foreach (var f in delayedLocalKeyAssignments)
                    {
                        f(localNamespace);
                    }
                }

                // Apply schema migrations where needed.
                @ref = _googleGlobalDatastoreModelMigration.ApplyMigrations(@namespace, @ref);

                return @ref;
            }
        }

        private Entity ToEntity<T>(string @namespace, T model, bool isCreateContext) where T : Model, new()
        {
            using (_managedTracer.StartSpan("DbSer:ToEntity<" + typeof(T).Name + ">()"))
            {
                var entity = new Entity();

                var types = model.GetTypes();
                var indexes = model.GetIndexes();
                foreach (var kv in types)
                {
                    var typeInfo = model.GetType().GetTypeInfo();
                    var propInfo = typeInfo.GetDeclaredProperty(kv.Key);
                    if (propInfo == null)
                    {
                        throw new InvalidOperationException($"The property '{kv.Key}' could not be found on '{typeInfo.FullName}'. Ensure the datastore type declarations are correct.");
                    }
                    var value = propInfo.GetValue(model);

                    switch (kv.Value)
                    {
                        case "string":
                            if (value == null)
                            {
                                entity[kv.Key] = new Value
                                {
                                    NullValue = NullValue.NullValue,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };
                            }
                            else
                            {
                                entity[kv.Key] = new Value
                                {
                                    StringValue = (string)value,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key) || ((string)value).Length > 700,
                                };
                            }
                            break;
                        case "boolean":
                            var boolNullable = (bool?)value;
                            if (!boolNullable.HasValue)
                            {
                                entity[kv.Key] = new Value
                                {
                                    NullValue = NullValue.NullValue,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };
                            }
                            else
                            {
                                entity[kv.Key] = new Value
                                {
                                    BooleanValue = boolNullable.Value,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };
                            }
                            break;
                        case "integer":
                            var longNullable = (long?)value;
                            if (!longNullable.HasValue)
                            {
                                entity[kv.Key] = new Value
                                {
                                    NullValue = NullValue.NullValue,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };
                            }
                            else
                            {
                                entity[kv.Key] = new Value
                                {
                                    IntegerValue = longNullable.Value,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };
                            }
                            break;
                        case "geopoint":
                            var geopoint = (LatLng)value;
                            if (geopoint == null)
                            {
                                entity[kv.Key] = new Value
                                {
                                    NullValue = NullValue.NullValue,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };

                                if (indexes.Contains(kv.Key))
                                {
                                    var geomodel = model as IGeoModel;
                                    if (geomodel != null)
                                    {
                                        var geopointFieldLengths = geomodel.GetHashKeyLengthsForGeopointFields();
                                        if (geopointFieldLengths.ContainsKey(kv.Key))
                                        {
                                            entity[kv.Key + GeoHashPropertySuffix] = new Value
                                            {
                                                NullValue = NullValue.NullValue,
                                                ExcludeFromIndexes = false,
                                            };
                                            entity[kv.Key + HashKeyPropertySuffix] = new Value
                                            {
                                                NullValue = NullValue.NullValue,
                                                ExcludeFromIndexes = false,
                                            };
                                        }
                                    }
                                }
                            }
                            else
                            {
                                entity[kv.Key] = new Value
                                {
                                    GeoPointValue = geopoint,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };

                                if (indexes.Contains(kv.Key))
                                {
                                    var geomodel = model as IGeoModel;
                                    if (geomodel != null)
                                    {
                                        var geopointFieldLengths = geomodel.GetHashKeyLengthsForGeopointFields();
                                        if (geopointFieldLengths.ContainsKey(kv.Key))
                                        {
                                            var geohash = S2Manager.GenerateGeohash(geopoint);
                                            var geohashkey = S2Manager.GenerateGeohashKey(geohash, geopointFieldLengths[kv.Key]);

                                            entity[kv.Key + GeoHashPropertySuffix] = new Value
                                            {
                                                StringValue = geohash.ToString(CultureInfo.InvariantCulture),
                                                ExcludeFromIndexes = false,
                                            };
                                            entity[kv.Key + HashKeyPropertySuffix] = new Value
                                            {
                                                IntegerValue = (long)geohashkey,
                                                ExcludeFromIndexes = false,
                                            };
                                        }
                                    }
                                }
                            }
                            break;
                        case "key":
                        case "local-key":
                        case "global-key":
                        case "unsafe-key":
                            var keyNullable = (Key)value;
                            if (keyNullable == null)
                            {
                                entity[kv.Key] = new Value
                                {
                                    NullValue = NullValue.NullValue,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };
                            }
                            else
                            {
                                if (kv.Value == "key")
                                {
                                    if (keyNullable.PartitionId.NamespaceId != @namespace)
                                    {
                                        throw new InvalidOperationException("Potential cross-namespace data write for key property '" + kv.Key + "'");
                                    }
                                }

                                if (kv.Value == "local-key")
                                {
                                    if (model.Key != null && model.Key.PartitionId.NamespaceId != string.Empty)
                                    {
                                        throw new InvalidOperationException("Attempted to use 'local-key' in entity that is not in the global namespace");
                                    }

                                    if (keyNullable.PartitionId.NamespaceId != model.GetDatastoreNamespaceForLocalKeys())
                                    {
                                        throw new InvalidOperationException(
                                            "Potential cross-namespace data write for key property '" + kv.Key +
                                            "' (got '" + keyNullable.PartitionId.NamespaceId + "', expected '" + model.GetDatastoreNamespaceForLocalKeys() + "')"
                                        );
                                    }
                                }

                                if (kv.Value == "global-key")
                                {
                                    if (model.Key != null && model.Key.PartitionId.NamespaceId == string.Empty)
                                    {
                                        throw new InvalidOperationException("Attempted to use 'global-key' in entity that is in the global namespace");
                                    }

                                    if (keyNullable.PartitionId.NamespaceId != string.Empty)
                                    {
                                        throw new InvalidOperationException("Potential cross-namespace data write for key property '" + kv.Key + "'");
                                    }
                                }

                                entity[kv.Key] = new Value
                                {
                                    KeyValue = keyNullable,
                                    ExcludeFromIndexes = !indexes.Contains(kv.Key),
                                };
                            }
                            break;
                        case "timestamp":
                            var instantNullable = (Instant?)value;
                            entity[kv.Key] = FromNodaTimeInstantToDatastoreValue(
                                instantNullable,
                                !indexes.Contains(kv.Key));
                            break;
                        case "json":
                            entity[kv.Key] = new Value
                            {
                                StringValue = JsonConvert.SerializeObject(value, new VersionedJsonConverter(), new NodaTimeInstantJsonConverter()),
                                ExcludeFromIndexes = true /* no meaningful way to search this data in Datastore */
                            };
                            break;
                        default:
                            throw new InvalidOperationException("Invalid type defined for property: " + kv.Value);
                    }
                }

                if (model.Key == null)
                {
                    var db = GetDatastoreForCurrentSite(@namespace);
                    var factory = db.CreateKeyFactory(model.GetKind());
                    entity.Key = factory.CreateIncompleteKey();
                }
                else
                {
                    entity.Key = model.Key;
                }

                var now = SystemClock.Instance.GetCurrentInstant();
                if (isCreateContext || model.dateCreatedUtc == null)
                {
                    model.dateCreatedUtc = now;
                }

                model.dateModifiedUtc = now;
                model.schemaVersion = model.GetSchemaVersion();

                entity["dateCreatedUtc"] = FromNodaTimeInstantToDatastoreValue(model.dateCreatedUtc, false);
                entity["dateModifiedUtc"] = FromNodaTimeInstantToDatastoreValue(model.dateModifiedUtc, false);
                entity["schemaVersion"] = model.schemaVersion;

                // hasImplicitMigrationsApplied is only for runtime checks so application code can see
                // if an entity was implicitly modified by migrations.

                return entity;
            }
        }

        private T FromJsonCache<T>(string @namespace, string jsonCache) where T : Model, new()
        {
            try
            {
                return _googleGlobalDatastoreModelSerialization.FromJson<T>(@namespace, jsonCache);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            // Caller should fallback to loading from datastore.
            throw new CacheInvalidException();
        }

        private string ToJsonCache<T>(T model) where T : Model, new()
        {
            return _googleGlobalDatastoreModelSerialization.ToJson(null, model);
        }

        private Instant? FromDatastoreValueToNodaTimeInstant(Value value)
        {
            return _instantTimestampConversion.FromDatastoreValueToNodaTimeInstant(value);
        }

        private Value FromNodaTimeInstantToDatastoreValue(Instant? instant, bool excludeFromIndexes)
        {
            return _instantTimestampConversion.FromNodaTimeInstantToDatastoreValue(instant, excludeFromIndexes);
        }
    }
}
