﻿using Google.Cloud.Datastore.V1;
using Grpc.Core;
using System.Linq;

namespace Redpoint.CloudFramework.Datastore
{
    public static class DatastoreExtensions
    {
        public static long GetIdFromKey(this Key key)
        {
            return key.Path.Last().Id;
        }

        public static string GetNameFromKey(this Key key)
        {
            return key.Path.Last().Name;
        }

        public static bool IsContentionException(this RpcException ex)
        {
            if (ex.Status.StatusCode == Grpc.Core.StatusCode.Aborted &&
                ex.Status.Detail.Contains("too much contention on these datastore entities. please try again."))
            {
                return true;
            }

            return false;
        }

        public static bool IsTransactionExpiryException(this RpcException ex)
        {
            if (ex.Status.StatusCode == Grpc.Core.StatusCode.InvalidArgument &&
                ex.Status.Detail.Contains("transaction has expired"))
            {
                return true;
            }

            return false;
        }
    }
}
