﻿using Google.Cloud.Datastore.V1;
using NodaTime;

namespace Redpoint.CloudFramework.Datastore
{
    public interface IInstantTimestampConversion
    {
        Instant? FromDatastoreValueToNodaTimeInstant(Value value);
        Value FromNodaTimeInstantToDatastoreValue(Instant? instant, bool excludeFromIndexes);
    }
}