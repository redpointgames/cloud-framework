﻿using System.Collections.Generic;
using Google.Cloud.Datastore.V1;
using Google.Protobuf;
using Redpoint.CloudFramework.Models;

namespace Redpoint.CloudFramework.Datastore
{
    public class MappedDatastoreQueryResults<T> where T : Model, new()
    {
        public ByteString EndCursor { get; set; }
        public string EndCursorForClients { get; set; }
        public QueryResultBatch.Types.MoreResultsType MoreResults { get; set; }
        public List<T> Entities { get; set; }
    }
}
