﻿using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Event;
using Redpoint.CloudFramework.Models;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Datastore
{
    public class GoogleGlobalDatastoreModelMigration : IGoogleGlobalDatastoreModelMigration
    {
        private readonly ICurrentEnvironment _currentEnvironment;
        private readonly IEventApi _eventApi;

        public GoogleGlobalDatastoreModelMigration(
            ICurrentEnvironment currentEnvironment,
            IEventApi eventApi)
        {
            _currentEnvironment = currentEnvironment;
            _eventApi = eventApi;
        }

        public T ApplyMigrations<T>(string @namespace, T model) where T : Model
        {
            if (model.schemaVersion == null)
            {
                // Default to version 1 for any existing models.
                model.schemaVersion = 1;
                model.hasImplicitMigrationsApplied = true;
            }

            if (model.GetSchemaVersion() != model.schemaVersion.Value)
            {
                var outcome = model.MigrateFromSchemaVersion(model.schemaVersion.Value);
                var newVersion = model.GetSchemaVersion();
                if (outcome == MigrationOutcome.ImplicitMigrationsApplied)
                {
                    model.schemaVersion = newVersion;
                    model.hasImplicitMigrationsApplied = true;
                }
                else if (outcome == MigrationOutcome.ExplicitMigrationRequired)
                {
                    Task.Run(async () =>
                    {
                        Key projectKey = null;
                        if (!string.IsNullOrWhiteSpace(@namespace))
                        {
                            projectKey = _currentEnvironment.GetTenantDatastoreKeyFromNamespace(@namespace);
                        }

                        await _eventApi.Raise(
                            CoreEventTypes.Migrate,
                            projectKey,
                            null,
                            null,
                            model.Key,
                            model,
                            new
                            {
                                oldVersion = model.schemaVersion ?? 0,
                                newVersion = newVersion,
                            });
                    });
                }
            }

            return model;
        }
    }
}
