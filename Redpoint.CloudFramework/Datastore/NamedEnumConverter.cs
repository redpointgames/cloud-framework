﻿using Newtonsoft.Json;
using Redpoint.CloudFramework.Infrastructure;
using System;
using System.Reflection;

namespace Redpoint.CloudFramework.Datastore
{
    public class NamedEnumConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            var t = (objectType.IsValueType && objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>))
                ? Nullable.GetUnderlyingType(objectType)
                : objectType;

            return t.IsEnum;
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null)
            {
                if (!(objectType.IsValueType && objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                {
                    throw new JsonSerializationException("Cannot convert null value to " + objectType.Name);
                }

                return null;
            }
            
            var enumType = (objectType.IsValueType && objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>))
                ? Nullable.GetUnderlyingType(objectType)
                : objectType;

            if (reader.TokenType == JsonToken.String)
            {
                var memberInfos = enumType.GetFields(BindingFlags.Public | BindingFlags.Static);
                foreach (var memberInfo in memberInfos)
                {
                    var attributes = memberInfo.GetCustomAttributes(typeof(NamedEnumValueAttribute), false);
                    var namedValue = ((NamedEnumValueAttribute)attributes[0]).Name;

                    if (namedValue == reader.Value.ToString())
                    {
                        return Enum.Parse(enumType, memberInfo.Name);
                    }
                }

                throw new JsonSerializationException("Unable to find mapped value for '" + reader.Value.ToString() + "'.");
            }

            throw new JsonSerializationException("Unexpected token when parsing enum.");
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            if (value == null)
            {
                writer.WriteNull();
                return;
            }

            var objectType = value.GetType();
            var enumType = (objectType.IsValueType && objectType.IsGenericType && objectType.GetGenericTypeDefinition() == typeof(Nullable<>))
                ? Nullable.GetUnderlyingType(objectType)
                : objectType;

            var fieldName = Enum.GetName(enumType, value);
            var memberInfo = enumType.GetMember(fieldName);
            var attributes = memberInfo[0].GetCustomAttributes(typeof(NamedEnumValueAttribute), false);
            var namedValue = ((NamedEnumValueAttribute)attributes[0]).Name;

            writer.WriteValue(namedValue);
        }
    }
}
