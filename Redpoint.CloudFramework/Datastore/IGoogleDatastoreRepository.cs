﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Models;

namespace Redpoint.CloudFramework.Datastore
{
    public interface IGoogleDatastoreRepository
    {
        Task<DatastoreDb> GetDatastoreDb();

        Task<ModelQuery<T>> CreateQuery<T>() where T : Model, new();

        Task<MappedDatastoreQueryResults<T>> RunUncachedQuery<T>(ModelQuery<T> query,
            ReadOptions.Types.ReadConsistency readConsistency, ModelTransaction transaction = null) where T : Model, new();

        Task<KeyFactory> GetKeyFactory(string kind);

        Task<KeyFactory> GetKeyFactory<T>() where T : Model, new();

        Task<Dictionary<Key, T>> LoadMany<T>(Key[] keys, ModelTransaction transaction = null) where T : Model, new();

        Task<T> LoadOneBy<T>(Key key, ModelTransaction transaction = null) where T : Model, new();

        Task<T> LoadOneBy<T, U>(string field, U value, ModelTransaction transaction = null) where T : Model, new();

        Task<List<T>> LoadAllBy<T, U>(string field, U value, ModelTransaction transaction = null) where T : Model, new();

        Task<List<T>> LoadAll<T>(ModelTransaction transaction = null) where T : Model, new();

        Task<List<T>> LoadAllUncached<T>(ModelTransaction transaction = null) where T : Model, new();

        Task Create<T>(T model, ModelTransaction transaction = null) where T : Model, new();

        Task<T[]> CreateMany<T>(IList<T> models) where T : Model, new();

        Task Upsert<T>(T model, ModelTransaction transaction = null) where T : Model, new();

        Task Update<T>(T model, ModelTransaction transaction = null) where T : Model, new();

        Task Delete<T>(T model, ModelTransaction transaction = null) where T : Model, new();

        Task<Key> AllocateKey<T>() where T : Model, new();

        Task<ModelTransaction> BeginTransaction();

        Task Commit(ModelTransaction transaction);

        Task Rollback(ModelTransaction transaction);
    }
}
