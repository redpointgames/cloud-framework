﻿using System;
using System.Security.Cryptography;

namespace Redpoint.CloudFramework.Infrastructure
{
    public class RandomStringGenerator : IRandomStringGenerator
    {
        private readonly RandomNumberGenerator _cryptoRng;

        public RandomStringGenerator()
        {
            _cryptoRng = RandomNumberGenerator.Create();
        }

        public string GetRandomString(int halfLength)
        {
            var bytes = new byte[halfLength];
            _cryptoRng.GetBytes(bytes);
            return BitConverter.ToString(bytes).Replace("-", "").ToLowerInvariant();
        }
    }
}
