﻿using System;

namespace Redpoint.CloudFramework.Infrastructure
{
    /// <summary>
    /// Associates a name with an enumeration value.
    /// </summary>
    [AttributeUsage(AttributeTargets.Field)]
    public class NamedEnumValueAttribute : Attribute
    {
        public NamedEnumValueAttribute(string name)
        {
            Name = name;
        }

        /// <summary>
        /// The name associated with the enumeration value.
        /// </summary>
        public string Name { get; private set; }
    }
}
