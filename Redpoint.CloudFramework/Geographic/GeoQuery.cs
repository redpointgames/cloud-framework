﻿using Google.Type;
using Redpoint.CloudFramework.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Redpoint.CloudFramework.Geographic
{
    public interface IGeoQuery
    {
        LatLng MinPoint { get; }

        LatLng MaxPoint { get; }

        IEnumerable<T> PostQueryFilter<T>(IEnumerable<T> results, Func<T, LatLng> expr) where T : Model, IGeoModel;
    }

    public class GeoRectangleQuery : IGeoQuery
    {
        public LatLng MinPoint { get; set; }

        public LatLng MaxPoint { get; set; }

        public IEnumerable<T> PostQueryFilter<T>(IEnumerable<T> results, Func<T, LatLng> expr) where T : Model, IGeoModel
        {
            // We don't need to perform any post filtering on rectangle queries.
            return results;
        }
    }

    public class GeoDistanceQuery : IGeoQuery
    {
        private LatLng _centerPoint;
        private float _distanceKm;
        private const double KM_TO_NM_UNIT = 1.0 / 1.852;
        private const double NM_TO_LATLNG_UNIT = 1.0 / 60.0;
        private const double KM_TO_LATLNG_UNIT = KM_TO_NM_UNIT * NM_TO_LATLNG_UNIT;

        private void RecomputeBounds()
        {
            MinPoint = new LatLng
            {
                Latitude = CenterPoint.Latitude - (_distanceKm * KM_TO_LATLNG_UNIT),
                Longitude = CenterPoint.Longitude - (_distanceKm * KM_TO_LATLNG_UNIT),
            };
            MaxPoint = new LatLng
            {
                Latitude = CenterPoint.Latitude + (_distanceKm * KM_TO_LATLNG_UNIT),
                Longitude = CenterPoint.Longitude + (_distanceKm * KM_TO_LATLNG_UNIT),
            };
        }

        public LatLng CenterPoint
        {
            get
            {
                return _centerPoint;
            }
            set
            {
                _centerPoint = value;
                RecomputeBounds();
            }
        }

        public float DistanceKm
        {
            get
            {
                return _distanceKm;
            }
            set
            {
                _distanceKm = value;
                RecomputeBounds();
            }
        }

        public LatLng MinPoint { get; private set; }

        public LatLng MaxPoint { get; private set; }

        public IEnumerable<T> PostQueryFilter<T>(IEnumerable<T> results, Func<T, LatLng> expr) where T : Model, IGeoModel
        {
            return results.Where(
                x => HaversineDistance(expr(x), _centerPoint) < _distanceKm);
        }

        private double HaversineDistance(LatLng pos1, LatLng pos2)
        {
            double R = 6371;
            var lat = (pos2.Latitude - pos1.Latitude) * (Math.PI / 180);
            var lng = (pos2.Longitude - pos1.Longitude) * (Math.PI / 180);
            var h1 = Math.Sin(lat / 2) * Math.Sin(lat / 2) +
                          Math.Cos(pos1.Latitude * (Math.PI / 180)) * Math.Cos(pos2.Latitude * (Math.PI / 180)) *
                          Math.Sin(lng / 2) * Math.Sin(lng / 2);
            var h2 = 2 * Math.Asin(Math.Min(1, Math.Sqrt(h1)));
            return R * h2;
        }
    }
}
