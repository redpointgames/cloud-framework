﻿using System.Collections.Generic;

namespace Redpoint.CloudFramework.Geographic
{
    public interface IGeoModel
    {
        Dictionary<string, ushort> GetHashKeyLengthsForGeopointFields();
    }
}
