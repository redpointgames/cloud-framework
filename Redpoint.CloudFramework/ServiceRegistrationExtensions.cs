﻿using Redpoint.CloudFramework.BigQuery;
using Redpoint.CloudFramework.Cache;
using Redpoint.CloudFramework.Datastore;
using Redpoint.CloudFramework.Error;
using Redpoint.CloudFramework.Event;
using Redpoint.CloudFramework.Event.PubSub;
using Redpoint.CloudFramework.Infrastructure;
using Redpoint.CloudFramework.Metric;
using Redpoint.CloudFramework.Prefix;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using System.Net;
using System;
using Redpoint.CloudFramework.Counter;
using StackExchange.Redis;
using Redpoint.CloudFramework.Locking;
using Microsoft.Extensions.Logging;
using System.Threading;
using Microsoft.AspNetCore.DataProtection;
using Redpoint.CloudFramework.DataProtection;
using Microsoft.Extensions.Caching.StackExchangeRedis;

namespace Redpoint.CloudFramework
{
    public static class ServiceRegistrationExtensions
    {
        private static void AddRedpointCommon(IServiceCollection services)
        {
            services.AddSingleton<IGoogleGlobalDatastoreRepository, GoogleGlobalDatastoreRepository>();
            services.AddSingleton<IGoogleGlobalDatastoreRepositoryHook[]>(svc => svc.GetServices<IGoogleGlobalDatastoreRepositoryHook>().ToArray());
            services.AddSingleton<IGoogleGlobalDatastoreModelSerialization, GoogleGlobalDatastoreModelSerialization>();
            services.AddSingleton<IGoogleGlobalDatastoreModelMigration, GoogleGlobalDatastoreModelMigration>();
            services.AddSingleton<IBigQuery, BigQuery.BigQuery>();
            services.AddSingleton<IErrorReporting, ErrorReporting>();
            services.AddSingleton<IEventApi, EventApi>();
            services.AddSingleton<IPubSub, GooglePubSub>();
            services.AddSingleton<IGlobalPrefix, GlobalPrefix>();
            services.AddSingleton<IRandomStringGenerator, RandomStringGenerator>();
            services.AddSingleton<IMetricService, MetricService>();
            services.AddSingleton<IGlobalLockService, DatastoreBasedGlobalLockService>();
            services.AddSingleton<IGoogleApiRetry, GoogleApiRetry>();
            services.AddSingleton<IInstantTimestampConversion, InstantTimestampConversion>();
        }

        public static void AddDistributedRedpointCache(
            this IServiceCollection services,
            ICurrentEnvironment currentEnvironment,
            ILogger logger,
            string redisInstanceName,
            ConnectionMultiplexer existingConnectionMultiplexer = null,
            Action<ConnectionMultiplexer> useRedisConnectionMultiplexer = null)
        {
            var redisServer = currentEnvironment.GetRedisServerName();
            if (string.IsNullOrWhiteSpace(redisServer))
            {
                services.AddDistributedMemoryCache();
                services.Add(ServiceDescriptor.Singleton<IDistributedCacheExtended, NullDistributedCacheExtended>());
            }
            else
            {
                var redisConnect = GetRedisConnectionString(currentEnvironment);

                services.AddOptions();
                services.Configure<RedisCacheOptions>(x =>
                {
                    x.InstanceName = redisInstanceName;
                    x.Configuration = redisConnect;
                });
                services.Add(ServiceDescriptor.Singleton<IDistributedCache, RetryableRedisCache>());
                services.Add(ServiceDescriptor.Singleton<IDistributedCacheExtended, DistributedCacheExtended>());

                var connectionMultiplexer = existingConnectionMultiplexer ?? ConnectToRedis(
                    currentEnvironment,
                    logger);

                services.AddSingleton(connectionMultiplexer);
                useRedisConnectionMultiplexer?.Invoke(connectionMultiplexer);

                // This is only usable if you require Redis.
                services.AddTransient<IShardedCounterService, ShardedCounterService>();
            }
        }

        public static string GetRedisConnectionString(ICurrentEnvironment currentEnvironment)
        {
            var redisServer = currentEnvironment.GetRedisServerName();

            if (string.IsNullOrWhiteSpace(redisServer))
            {
                return null;
            }

            var serverName = redisServer;
            var portSuffix = string.Empty;
            if (serverName.Contains(":"))
            {
                var s = serverName.Split(new[] { ':' }, 2);
                serverName = s[0];
                portSuffix = ":" + s[1];
            }

            string redisConnect;
            if (IPAddress.TryParse(serverName, out var address))
            {
                // Server component is a direct network address; use
                // original Redis server string as-is.
                redisConnect = redisServer;
            }
            else
            {
                // Lookup based on DNS.
                var dnsTask = Dns.GetHostAddressesAsync(serverName);
                var addresses = dnsTask.Result;
                redisConnect = string.Join(",", addresses.Where(x => x.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork).Select(x => x.MapToIPv4().ToString() + portSuffix));
            }

            return redisConnect;
        }

        public static ConnectionMultiplexer ConnectToRedis(
            ICurrentEnvironment currentEnvironment,
            ILogger logger)
        {
            var redisConnect = GetRedisConnectionString(currentEnvironment);

            if (redisConnect == null)
            {
                return null;
            }

            ConnectionMultiplexer connectionMultiplexer = null;
            var now = DateTime.Now;
            for (var i = 0; i < 30; i++)
            {
                try
                {
                    logger?.LogInformation($"Attempting to connect to Redis (attempt #{i + 1})...");
                    connectionMultiplexer = ConnectionMultiplexer.Connect(redisConnect);
                    break;
                }
                catch (RedisConnectionException ex)
                {
                    logger?.LogWarning($"Failed to connect to Redis (attempt #{i + 1}), waiting {i} seconds before trying again...");
                    Thread.Sleep(i * 1000);
                    continue;
                }
            }

            if (connectionMultiplexer == null)
            {
                logger?.LogCritical($"Unable to connect to Redis after 30 attempts and {Math.Round((DateTime.Now - now).TotalMinutes, 0).ToString()} minutes... something is drastically wrong!");
                throw new ApplicationException("Unable to connect to Redis!");
            }

            return connectionMultiplexer;
        }

        public static void AddRedpointCommonWeb(this IServiceCollection services)
        {
            AddRedpointCommon(services);

            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<IGoogleDatastoreRepository, GoogleDatastoreRepository>();
            services.AddScoped<ILockService, DefaultLockService>();
            services.AddScoped<IPrefix, Prefix.Prefix>();

            services.AddSingleton<IDataProtectionProvider, StaticDataProtectionProvider>();
            services.AddSingleton<IDataProtector, StaticDataProtector>();
        }

        public static void AddRedpointCommonProcessor(this IServiceCollection services)
        {
            AddRedpointCommon(services);
        }
    }
}
