﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Security.Cryptography;

namespace Redpoint.CloudFramework.DataProtection
{
    /// <summary>
    /// We don't protect anything sensitive (ASP.NET Core just needs some encryption for the session key
    /// that it stores in cookies), and all of the built-in data protection mechanisms are unreliable.
    /// 
    /// You need to create AES key/IV settings to your appsettings.json:
    /// 
    /// {
    ///   "CloudFramework": {
    ///     "Security": {
    ///       "AES": {
    ///         "Key": "",
    ///         "IV": ""
    ///       }
    ///     }
    ///   }
    /// }
    /// 
    /// If you run your application unconfigured, the framework will throw an exception that you can
    /// get newly generated values out of to use in appsettings.json.
    /// </summary>
    public class StaticDataProtector : IDataProtector
    {
        private readonly Aes _aes;
        private byte[] _aesKey;
        private byte[] _aesIV;

        public StaticDataProtector(IConfiguration configuration, ILogger<StaticDataProtector> logger)
        {
            _aes = Aes.Create();
            _aes.BlockSize = 128;
            _aes.Mode = CipherMode.CBC;
            _aes.Padding = PaddingMode.PKCS7;

            // If the developer is running their app unconfigured, generate the key and IV and throw an exception with
            // the values to make it easy to set values into appsettings.json
            if (string.IsNullOrEmpty(configuration["CloudFramework:Security:AES:Key"]) ||
                string.IsNullOrEmpty(configuration["CloudFramework:Security:AES:IV"]))
            {
                _aes.GenerateIV();
                _aes.GenerateKey();
                var message = "You haven't set the AES key/IV in appsettings.json. Here are newly generated values for you. Key: '" + Convert.ToBase64String(_aes.Key) + "', IV: '" + Convert.ToBase64String(_aes.IV) + "'. Refer to documentation on how to set this up.";
                logger.LogError(message);
                throw new Exception(message);
            }

            _aesKey = Convert.FromBase64String(configuration["CloudFramework:Security:AES:Key"]);
            _aesIV = Convert.FromBase64String(configuration["CloudFramework:Security:AES:IV"]);
        }

        public IDataProtector CreateProtector(string purpose)
        {
            return this;
        }

        public byte[] Protect(byte[] plaintext)
        {
            using (var encryptor = _aes.CreateEncryptor(_aesKey, _aesIV))
            {
                using (var result = new MemoryStream())
                {
                    using (var stream = new CryptoStream(result, encryptor, CryptoStreamMode.Write, true))
                    {
                        stream.Write(plaintext, 0, plaintext.Length);
                    }

                    var l = new byte[result.Position];
                    result.Seek(0, SeekOrigin.Begin);
                    result.Read(l, 0, l.Length);
                    return l;
                }
            }
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            using (var decryptor = _aes.CreateDecryptor(_aesKey, _aesIV))
            {
                using (var result = new MemoryStream())
                {
                    using (var stream = new CryptoStream(result, decryptor, CryptoStreamMode.Write, true))
                    {
                        stream.Write(protectedData, 0, protectedData.Length);
                    }

                    var l = new byte[result.Position];
                    result.Seek(0, SeekOrigin.Begin);
                    result.Read(l, 0, l.Length);
                    return l;
                }
            }
        }
    }
}
