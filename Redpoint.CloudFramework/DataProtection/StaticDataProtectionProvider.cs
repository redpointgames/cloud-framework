﻿using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace Redpoint.CloudFramework.DataProtection
{
    public class StaticDataProtectionProvider : IDataProtectionProvider
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<StaticDataProtector> _logger;

        public StaticDataProtectionProvider(IConfiguration configuration, ILogger<StaticDataProtector> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        public IDataProtector CreateProtector(string purpose)
        {
            return new StaticDataProtector(_configuration, _logger);
        }
    }
}
