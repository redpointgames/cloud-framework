﻿using System.Threading.Tasks;
using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Models;

namespace Redpoint.CloudFramework.Prefix
{
    public class Prefix : IPrefix
    {
        private readonly ICurrentTenantService _currentProjectService;
        private readonly IGlobalPrefix _globalPrefix;

        public Prefix(ICurrentTenantService currentProjectService, IGlobalPrefix globalPrefix)
        {
            _currentProjectService = currentProjectService;
            _globalPrefix = globalPrefix;
        }

        public string Create(Key key)
        {
            return _globalPrefix.Create(key);
        }

        public string CreateInternal(Key key)
        {
            return _globalPrefix.CreateInternal(key);
        }

        public async Task<Key> Parse(string identifier)
        {
            return _globalPrefix.Parse((await _currentProjectService.GetTenant()).DatastoreNamespace, identifier);
        }

        public async Task<Key> ParseInternal(string identifier)
        {
            return _globalPrefix.ParseInternal((await _currentProjectService.GetTenant()).DatastoreNamespace, identifier);
        }

        public async Task<Key> ParseLimited(string identifier, string kind)
        {
            return _globalPrefix.ParseLimited((await _currentProjectService.GetTenant()).DatastoreNamespace, identifier, kind);
        }

        public async Task<Key> ParseLimited<T>(string identifier) where T : Model, new()
        {
            return _globalPrefix.ParseLimited<T>((await _currentProjectService.GetTenant()).DatastoreNamespace, identifier);
        }
    }
}
