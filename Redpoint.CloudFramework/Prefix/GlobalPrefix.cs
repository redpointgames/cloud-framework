﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Google.Cloud.Datastore.V1;
using Redpoint.CloudFramework.Error;
using Redpoint.CloudFramework.Models;

namespace Redpoint.CloudFramework.Prefix
{
    public class GlobalPrefix : IGlobalPrefix
    {
        private readonly Dictionary<string, string> _prefixes;
        private readonly Dictionary<string, string> _reversePrefixes;
        private readonly ICommonExceptionFactory _commonExceptionFactory;
        private readonly ICurrentEnvironment _currentEnvironment;

        public GlobalPrefix(
            IPrefixProvider prefixProvider,
            ICurrentEnvironment currentEnvironment,
            ICommonExceptionFactory commonExceptionFactory)
        {
            _commonExceptionFactory = commonExceptionFactory;
            _currentEnvironment = currentEnvironment;

            _prefixes = prefixProvider.Prefixes;
            _reversePrefixes = _prefixes.ToDictionary(k => k.Value, v => v.Key);
        }

        /// <summary>
        /// Parse a public identifier into a Google Datastore key object.
        /// </summary>
        /// <param name="datastoreNamespace">The datastore namespace of the resulting key.</param>
        /// <param name="identifier">The identifier to parse.</param>
        /// <returns>A key object.</returns>
        public Key Parse(string datastoreNamespace, string identifier)
        {
            var prefix = ParsePathElement(identifier);

            var k = new Key
            {
                PartitionId = new PartitionId(_currentEnvironment.GetGoogleProjectId(), datastoreNamespace)
            };
            k.Path.Add(prefix);
            return k;
        }

        /// <summary>
        /// Parse a public identifier into a Google Datastore key object and verify it's kind.
        /// </summary>
        /// <param name="datastoreNamespace">The datastore namespace of the resulting key.</param>
        /// <param name="identifier">The identifier to parse.</param>
        /// <param name="kind">The resulting kind that the key must match.</param>
        /// <returns>A key object.</returns>
        public Key ParseLimited(string datastoreNamespace, string identifier, string kind)
        {
            if (string.IsNullOrWhiteSpace(kind))
            {
                throw new ArgumentNullException(nameof(kind));
            }

            var result = Parse(datastoreNamespace, identifier);
            if (result.Path.Last().Kind != kind)
            {
                throw _commonExceptionFactory.IdentifierWrongTypeError(identifier, kind);
            }

            return result;
        }

        /// <summary>
        /// Parse a public identifier into a Google Datastore key object and verify it's kind.
        /// </summary>
        /// <typeparam name="T">The datastore model that this must match.</typeparam>
        /// <param name="datastoreNamespace">The datastore namespace of the resulting key.</param>
        /// <param name="identifier">The identifier to parse.</param>
        /// <returns>A key object.</returns>
        public Key ParseLimited<T>(string datastoreNamespace, string identifier) where T : Model, new()
        {
            return ParseLimited(datastoreNamespace, identifier, new T().GetKind());
        }

        /// <summary>
        /// Parse an internal or public identifier into a Google Datastore key.
        /// </summary>
        /// <param name="datastoreNamespace">The datastore namespace of the resulting key.</param>
        /// <param name="identifier">The identifier to parse.</param>
        /// <returns>A key object.</returns>
        public Key ParseInternal(string datastoreNamespace, string identifier)
        {
            if (string.IsNullOrWhiteSpace(identifier))
            {
                throw new ArgumentNullException(nameof(identifier));
            }

            var isNamespacedKey = false;
            if (identifier.StartsWith("#"))
            {
                isNamespacedKey = true;
                identifier = identifier.Substring(1);
            }

            var identifiers = ParsePipeSeperated(identifier);
            var pathElements = new List<Key.Types.PathElement>();

            string projectId;
            string namespaceId;
            int offset = 0;

            if (isNamespacedKey)
            {
                if (identifiers[0] != "v1")
                {
                    throw new ArgumentException("Namespaced key is not a supported version");
                }

                projectId = identifiers[1];
                namespaceId = identifiers[2];

                offset = 3;
            }
            else
            {
                projectId = _currentEnvironment.GetGoogleProjectId();
                namespaceId = datastoreNamespace;
            }

            for (var i = offset; i < identifiers.Length; i++)
            {
                var component = identifiers[i];
                var colonIndex = component.IndexOf(':');
                if (colonIndex != -1)
                {
                    var kind = component.Substring(0, colonIndex);
                    var ident = component.Substring(colonIndex + 1);
                    if (ident.StartsWith("id="))
                    {
                        var id = long.Parse(ident.Substring("id=".Length));
                        pathElements.Add(new Key.Types.PathElement(kind, id));
                    }
                    else if (ident.StartsWith("name="))
                    {
                        var name = ident.Substring("name=".Length);
                        pathElements.Add(new Key.Types.PathElement(kind, name));
                    }
                    else
                    {
                        throw new Exception("Unknown ID or name for identifier '" + component + "'");
                    }
                }
                else
                {
                    pathElements.Add(ParsePathElement(component));
                }
            }

            var k = new Key
            {
                PartitionId = new PartitionId(projectId, namespaceId)
            };
            k.Path.AddRange(pathElements);
            return k;
        }

        /// <summary>
        /// Creates a public identifier from a Datastore key.
        /// </summary>
        /// <param name="key">The datastore key to create an identifier from.</param>
        /// <returns>The public identifier.</returns>
        public string Create(Key key)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }
            if (key.Path.Count == 0)
            {
                throw new Exception("Datastore key does not have any path elements; can not generate public identifier");
            }
            if (key.Path.Count > 1)
            {
                throw new Exception("Datastore key has more than one path element (nested children), can not generate public identifier");
            }
            return CreatePathElement(key.Path[0]);
        }

        /// <summary>
        /// Creates a public or internal identifier from a Datastore key.
        /// </summary>
        /// <param name="key">The datastore key to create an identifier from.</param>
        /// <returns>The public or internal identifier.</returns>
        public string CreateInternal(Key key, PathGenerationMode pathGenerationMode = PathGenerationMode.Default)
        {
            if (key == null)
            {
                throw new ArgumentNullException(nameof(key));
            }

            var keyComponents = new List<string>();

            keyComponents.Add("v1");
            keyComponents.Add(key.PartitionId.ProjectId);
            keyComponents.Add(key.PartitionId.NamespaceId);

            for (var i = 0; i < key.Path.Count; i++)
            {
                var pathElement = key.Path[i];

                if (!_reversePrefixes.ContainsKey(pathElement.Kind) || 
                    pathGenerationMode == PathGenerationMode.NoShortPathComponents ||
                    pathElement.IdTypeCase != Key.Types.PathElement.IdTypeOneofCase.Id)
                {
                    if (pathElement.IdTypeCase == Key.Types.PathElement.IdTypeOneofCase.Id)
                    {
                        if (pathElement.Id <= 0)
                        {
                            throw new Exception("Numeric component must be a positive value");
                        }
                    }

                    if (pathElement.IdTypeCase == Key.Types.PathElement.IdTypeOneofCase.Name)
                    {
                        keyComponents.Add(pathElement.Kind + ":name=" + pathElement.Name.Replace("\\", "\\\\").Replace("|", "\\|"));
                    }
                    else
                    {
                        keyComponents.Add(pathElement.Kind + ":id=" + pathElement.Id);
                    }
                }
                else
                {
                    keyComponents.Add(CreatePathElement(pathElement));
                }
            }
            
            return "#" + string.Join("|", keyComponents);
        }

        private string[] ParsePipeSeperated(string value)
        {
            var results = new List<string>();
            var buffer = string.Empty;
            var isEscaped = false;
            for (var v = 0; v < value.Length; v++)
            {
                if (isEscaped)
                {
                    buffer += value[v];
                    isEscaped = false;
                }
                else
                {
                    if (value[v] == '\\')
                    {
                        isEscaped = true;
                        continue;
                    }

                    if (value[v] == '|')
                    {
                        results.Add(buffer);
                        buffer = string.Empty;
                        continue;
                    }

                    buffer += value[v];
                }
            }
            if (buffer.Length > 0)
            {
                results.Add(buffer);
            }
            return results.ToArray();
        }

        private Key.Types.PathElement ParsePathElement(string identifier)
        {
            if (identifier == null)
            {
                throw new ArgumentNullException(nameof(identifier));
            }

            var components = identifier.Split(new[] { '-' }, 2);
            if (components.Length != 2)
            {
                throw _commonExceptionFactory.IdentifierInvalidError(identifier, "Missing seperator in identifier");
            }

            var prefix = components[0].ToLowerInvariant();
            if (!_prefixes.ContainsKey(prefix))
            {
                throw _commonExceptionFactory.IdentifierInvalidError(identifier, "Unknown prefix in identifier");
            }
            if (string.IsNullOrWhiteSpace(components[1]))
            {
                throw _commonExceptionFactory.IdentifierInvalidError(identifier, "Missing numeric component to identifier");
            }
            var parsable = long.TryParse(components[1], NumberStyles.Any, CultureInfo.InvariantCulture,
                out var numericIdentifier);
            if (!parsable || numericIdentifier.ToString(CultureInfo.InvariantCulture) != components[1])
            {
                throw _commonExceptionFactory.IdentifierInvalidError(identifier, "Badly formatted numeric component in identifier");
            }

            return new Key.Types.PathElement(_prefixes[prefix], numericIdentifier);
        }

        private string CreatePathElement(Key.Types.PathElement pathElement)
        {
            if (string.IsNullOrWhiteSpace(pathElement.Kind))
            {
                throw new Exception("Kind property on datastore key is null or empty");
            }
            if (!_reversePrefixes.ContainsKey(pathElement.Kind))
            {
                throw new Exception("No prefix for object kind: " + pathElement.Kind);
            }
            if (pathElement.IdTypeCase != Key.Types.PathElement.IdTypeOneofCase.Id)
            {
                throw new Exception("Only numeric based Datastore keys can be publicly prefixed");
            }
            if (pathElement.Id < 0)
            {
                throw new Exception("Numeric component must be a positive value");
            }
            return _reversePrefixes[pathElement.Kind] + "-" + pathElement.Id;
        }
    }
}
