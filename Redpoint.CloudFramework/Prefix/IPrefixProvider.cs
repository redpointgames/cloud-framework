﻿using System.Collections.Generic;

namespace Redpoint.CloudFramework.Prefix
{
    public interface IPrefixProvider
    {
        Dictionary<string, string> Prefixes { get; }
    }
}
