﻿using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Cache
{
    public class RetryableRedisCache : IDistributedCache, IDisposable
    {
        private readonly RedisCache _cache;
        private const int RetryCount = 10;

        public RetryableRedisCache(IOptions<RedisCacheOptions> redisOptions)
        {
            _cache = new RedisCache(redisOptions);
        }

        public void Dispose()
        {
            _cache.Dispose();
        }

        public byte[] Get(string key)
        {
            return Retry(() => _cache.Get(key));
        }

        public Task<byte[]> GetAsync(string key)
        {
            return RetryAsync(() => _cache.GetAsync(key));
        }

        public void Refresh(string key)
        {
            Retry(() => _cache.Refresh(key));
        }

        public Task RefreshAsync(string key)
        {
            return RetryAsync(() => _cache.RefreshAsync(key));
        }

        public void Remove(string key)
        {
            Retry(() => _cache.Remove(key));
        }

        public Task RemoveAsync(string key)
        {
            return RetryAsync(() => _cache.RemoveAsync(key));
        }

        public void Set(string key, byte[] value, DistributedCacheEntryOptions options)
        {
            Retry(() => _cache.Set(key, value, options));
        }

        public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options)
        {
            return RetryAsync(() => _cache.SetAsync(key, value, options));
        }

        public Task<byte[]> GetAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return RetryAsync(() => _cache.GetAsync(key, token));
        }

        public Task SetAsync(string key, byte[] value, DistributedCacheEntryOptions options, CancellationToken token = default(CancellationToken))
        {
            return RetryAsync(() => _cache.SetAsync(key, value, options, token));
        }

        public Task RefreshAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return RetryAsync(() => _cache.RefreshAsync(key, token));
        }

        public Task RemoveAsync(string key, CancellationToken token = default(CancellationToken))
        {
            return RetryAsync(() => _cache.RemoveAsync(key, token));
        }

        #region Retry Logic

        private void Retry(Action func)
        {
            for (var i = 0; i < RetryCount; i++)
            {
                try
                {
                    func();
                    return;
                }
                catch (NullReferenceException ex)
                {
                    // Bug in Microsoft.Extensions.Caching.Redis! See https://github.com/aspnet/Caching/issues/270.
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    Thread.Sleep(i * 100);
                    continue;
                }
                catch (TimeoutException)
                {
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    Thread.Sleep(i * 100);
                    continue;
                }
            }

            throw new InvalidOperationException("Should not be able to escape for loop in Retry() of RetryableRedisCache");
        }

        private T Retry<T>(Func<T> func)
        {
            for (var i = 0; i < RetryCount; i++)
            {
                try
                {
                    return func();
                }
                catch (NullReferenceException ex)
                {
                    // Bug in Microsoft.Extensions.Caching.Redis! See https://github.com/aspnet/Caching/issues/270.
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    Thread.Sleep(i * 100);
                    continue;
                }
                catch (TimeoutException)
                {
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    Thread.Sleep(i * 100);
                    continue;
                }
            }

            throw new InvalidOperationException("Should not be able to escape for loop in Retry<T>() of RetryableRedisCache");
        }

        private async Task RetryAsync(Func<Task> func)
        {
            for (var i = 0; i < RetryCount; i++)
            {
                try
                {
                    await func();
                    return;
                }
                catch (NullReferenceException ex)
                {
                    // Bug in Microsoft.Extensions.Caching.Redis! See https://github.com/aspnet/Caching/issues/270.
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    await Task.Delay(i * 100);
                    continue;
                }
                catch (TimeoutException)
                {
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    await Task.Delay(i * 100);
                    continue;
                }
            }

            throw new InvalidOperationException("Should not be able to escape for loop in RetryAsync() of RetryableRedisCache");
        }

        private async Task<T> RetryAsync<T>(Func<Task<T>> func)
        {
            for (var i = 0; i < RetryCount; i++)
            {
                try
                {
                    return await func();
                }
                catch (NullReferenceException ex)
                {
                    // Bug in Microsoft.Extensions.Caching.Redis! See https://github.com/aspnet/Caching/issues/270.
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    await Task.Delay(i * 100);
                    continue;
                }
                catch (TimeoutException)
                {
                    if (i == RetryCount - 1)
                    {
                        throw;
                    }

                    await Task.Delay(i * 100);
                    continue;
                }
            }

            throw new InvalidOperationException("Should not be able to escape for loop in RetryAsync<T>() of RetryableRedisCache");
        }

        #endregion
    }
}
