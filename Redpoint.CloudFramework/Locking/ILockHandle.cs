﻿using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Locking
{
    public interface ILockHandle
    {
        Task Release();
    }
}
