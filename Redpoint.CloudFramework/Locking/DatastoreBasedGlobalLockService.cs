﻿using Google.Cloud.Datastore.V1;
using Microsoft.Extensions.Logging;
using NodaTime;
using Redpoint.CloudFramework.Datastore;
using Redpoint.CloudFramework.Error;
using Redpoint.CloudFramework.Metric;
using Redpoint.CloudFramework.Models;
using Redpoint.CloudFramework.Prefix;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Locking
{
    public class LockAcquisitionException : Exception
    {
        public LockAcquisitionException(string lockId) : base("Unable to acquire lock: " + lockId + ", already in use.")
        {
        }
    }

    public class DatastoreBasedGlobalLockService : IGlobalLockService
    {
        private readonly IGlobalPrefix _globalPrefix;
        private readonly ILogger<DatastoreBasedGlobalLockService> _logger;
        private readonly IGoogleGlobalDatastoreRepository _googleGlobalDatastoreRepository;
        private readonly ICommonExceptionFactory _commonExceptionFactory;
        private readonly IMetricService _metricService;

        private static readonly Duration DefaultExpiryDuration = Duration.FromMinutes(5);
        private static readonly Duration DefaultRenewalDuration = Duration.FromSeconds(60);

        private const string LockAcquireMetric = "rcf/lock_acquire_count";
        private const string LockContentionFailureMetric = "rcf/lock_contention_failure_count";
        private const string LockRenewedMetric = "rcf/lock_renewed_count";
        private const string LockReleaseMetric = "rcf/lock_release_count";

        public DatastoreBasedGlobalLockService(
            IGlobalPrefix globalPrefix,
            ILogger<DatastoreBasedGlobalLockService> logger,
            IGoogleGlobalDatastoreRepository googleGlobalDatastoreRepository,
            ICommonExceptionFactory commonExceptionFactory,
            IMetricService metricService)
        {
            _globalPrefix = globalPrefix;
            _logger = logger;
            _googleGlobalDatastoreRepository = googleGlobalDatastoreRepository;
            _commonExceptionFactory = commonExceptionFactory;
            _metricService = metricService;
        }

        public async Task<ILockHandle> Acquire(string @namespace, Key objectToLock)
        {
            var objectToLockName = _globalPrefix.CreateInternal(objectToLock);
            _logger?.LogInformation("Beginning acquisition of lock {0}/{1}...", @namespace, objectToLockName);
            var lockKey = await _googleGlobalDatastoreRepository.CreateNamedKey<DefaultLockModel>(@namespace, objectToLockName);
            var transaction = await _googleGlobalDatastoreRepository.BeginTransaction(@namespace);
            var acquisitionGuid = Guid.NewGuid().ToString();
            _logger?.LogInformation("{0}: Begun transaction for {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);
            var doRollback = false;
            try
            {
                _logger?.LogInformation("{0}: Loading existing lock model for {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);
                var existingLock = await _googleGlobalDatastoreRepository.LoadOneBy<DefaultLockModel>(@namespace, lockKey, transaction);
                if (existingLock == null)
                {
                    _logger?.LogInformation("{0}: No existing lock object for {1}/{2}, creating new lock model...", acquisitionGuid, @namespace, objectToLockName);

                    // No existing lock, use create semantics.
                    existingLock = new DefaultLockModel
                    {
                        Key = lockKey,
                        acquisitionGuid = acquisitionGuid,
                        dateExpiresUtc = SystemClock.Instance.GetCurrentInstant().Plus(DefaultExpiryDuration),
                    };

                    await _googleGlobalDatastoreRepository.Create(@namespace, existingLock, transaction);
                }
                else
                {
                    _logger?.LogInformation("{0}: Found existing lock object for {1}/{2}, checking expiry...", acquisitionGuid, @namespace, objectToLockName);

                    // Existing lock, check if expired and use update semantics.
                    if (existingLock.dateExpiresUtc <= SystemClock.Instance.GetCurrentInstant())
                    {
                        _logger?.LogInformation("{0}: Existing lock {1}/{2} has naturally expired, taking...", acquisitionGuid, @namespace, objectToLockName);

                        // Lock expired, we can take. Update the acquisition GUID (so the original owner
                        // knows they lost the lock if the attempt to renew it).
                        existingLock.acquisitionGuid = acquisitionGuid;
                        existingLock.dateExpiresUtc = SystemClock.Instance.GetCurrentInstant().Plus(DefaultExpiryDuration);

                        await _googleGlobalDatastoreRepository.Update(@namespace, existingLock, transaction);
                    }
                    else
                    {
                        _logger?.LogInformation("{0}: Existing lock {1}/{2} still in use, throwing..", acquisitionGuid, @namespace, objectToLockName);

                        await _metricService.AddPoint(
                            LockContentionFailureMetric,
                            1,
                            null,
                            new Dictionary<string, string>
                            {
                                { "namespace", @namespace ?? string.Empty },
                                { "object_kind", objectToLock.Path.Last().Kind },
                            });

                        throw new LockAcquisitionException(objectToLockName);
                    }
                }

                _logger?.LogInformation("{0}: Attempting commit of transaction for {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);
                await _googleGlobalDatastoreRepository.Commit(@namespace, transaction);
                doRollback = false;
                _logger?.LogInformation("{0}: Successful commit of transaction for {1}/{2}, returning lock handle...", acquisitionGuid, @namespace, objectToLockName);

                await _metricService.AddPoint(
                    LockAcquireMetric,
                    1,
                    null,
                    new Dictionary<string, string>
                    {
                        { "namespace", @namespace ?? string.Empty },
                        { "object_kind", objectToLock.Path.Last().Kind },
                    });

                return new LockHandle(
                    _googleGlobalDatastoreRepository,
                    _logger,
                    _metricService,
                    existingLock.Key,
                    @namespace,
                    acquisitionGuid,
                    objectToLock.Path.Last().Kind);
            }
            catch (LockAcquisitionException)
            {
                // Just rethrow, we already logged why this was happening.
                throw;
            }
            catch (Exception ex)
            {
                if (_commonExceptionFactory.IsRetryRequestError(ex))
                {
                    _logger?.LogInformation("{0}: Encountered lock contention while acquiring {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);

                    await _metricService.AddPoint(
                        LockContentionFailureMetric,
                        1,
                        null,
                        new Dictionary<string, string>
                        {
                            { "namespace", @namespace ?? string.Empty },
                            { "object_kind", objectToLock.Path.Last().Kind },
                        });

                    throw new LockAcquisitionException(objectToLockName);
                }
                else
                {
                    _logger?.LogCritical(ex, "{0}: Exception during acquisition of {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);
                    throw;
                }
            }
            finally
            {
                _logger?.LogInformation("{0}: Reached finally block for {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);
                if (doRollback)
                {
                    _logger?.LogInformation("{0}: Attempting rollback of transaction for {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);
                    await _googleGlobalDatastoreRepository.Rollback(@namespace, transaction);
                    _logger?.LogInformation("{0}: Transaction rollback completed for {1}/{2}...", acquisitionGuid, @namespace, objectToLockName);
                }
            }
        }

        public async Task AcquireAndUse(string @namespace, Key objectToLock, Func<Task> block)
        {
            var handle = await Acquire(@namespace, objectToLock);
            try
            {
                await block();
            }
            finally
            {
                await handle.Release();
            }
        }

        public async Task<T> AcquireAndUse<T>(string @namespace, Key objectToLock, Func<Task<T>> block)
        {
            var handle = await Acquire(@namespace, objectToLock);
            try
            {
                return await block();
            }
            finally
            {
                await handle.Release();
            }
        }

        private class LockHandle : ILockHandle
        {
            private readonly IGoogleGlobalDatastoreRepository _googleGlobalDatastoreRepository;
            private readonly ILogger<DatastoreBasedGlobalLockService> _logger;
            private readonly IMetricService _metricService;
            private readonly string _objectKind;
            private readonly Key _lockKey;
            private readonly string _namespace;
            private readonly string _acquisitionGuid;
            private readonly CancellationTokenSource _cancellationTokenSource;
            private bool _isReleased;

            public LockHandle(
                IGoogleGlobalDatastoreRepository googleGlobalDatastoreRepository,
                ILogger<DatastoreBasedGlobalLockService> logger,
                IMetricService metricService,
                Key realLockKey,
                string @namespace,
                string acquisitionGuid,
                string objectKind)
            {
                _googleGlobalDatastoreRepository = googleGlobalDatastoreRepository;
                _logger = logger;
                _lockKey = realLockKey;
                _namespace = @namespace;
                _acquisitionGuid = acquisitionGuid;
                _cancellationTokenSource = new CancellationTokenSource();
                _isReleased = false;
                _metricService = metricService;
                _objectKind = objectKind;

                _logger?.LogInformation("{0}: Lock handle created for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                Task.Run(AutomaticRenewal);
            }

            private async Task AutomaticRenewal()
            {
                _logger?.LogInformation("{0}: Automatic renewal task running for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                while (!_isReleased)
                {
                    _logger?.LogInformation("{0}: Lock handle is not released {1}/{2}, delaying for {3}ms...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey(), (int)DefaultRenewalDuration.TotalMilliseconds);

                    await Task.Delay((int)DefaultRenewalDuration.TotalMilliseconds, _cancellationTokenSource.Token);

                    if (_isReleased)
                    {
                        _logger?.LogInformation("{0}: Lock handle was released since renewal delay began for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                        await _metricService.AddPoint(
                            LockReleaseMetric,
                            1,
                            null,
                            new Dictionary<string, string>
                            {
                                { "namespace", _namespace ?? string.Empty },
                                { "object_kind", _objectKind },
                            });

                        return;
                    }

                    _logger?.LogInformation("{0}: Beginning renewal transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                    var transaction = await _googleGlobalDatastoreRepository.BeginTransaction(_namespace);
                    _logger?.LogInformation("{0}: Begun renewal transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                    var doRollback = false;
                    try
                    {
                        _logger?.LogInformation("{0}: Loading existing lock model for {1}/{2} (renewal)...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                        var existingLock = await _googleGlobalDatastoreRepository.LoadOneBy<DefaultLockModel>(_namespace, _lockKey, transaction);
                        if (existingLock == null)
                        {
                            _logger?.LogWarning("{0}: Unreleased lock {1}/{2} during renewal appears to have been acquired and released by someone else!", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                            await _metricService.AddPoint(
                                LockReleaseMetric,
                                1,
                                null,
                                new Dictionary<string, string>
                                {
                                    { "namespace", _namespace ?? string.Empty },
                                    { "object_kind", _objectKind },
                                });

                            // No lock? what? Assume someone else took control of the lock because
                            // we let it lapse, and then they were finished with it so they deleted it.
                            // In this case, the lock has been released due to expiry, so bail.
                            _isReleased = true;
                            return;
                        }
                        else
                        {
                            // Existing lock, check if we still have the handle on it.
                            if (existingLock.acquisitionGuid == _acquisitionGuid)
                            {
                                _logger?.LogInformation("{0}: Updating the expiry time on {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                                // Update the lock's expiry time to our current time plus the default expiry.
                                existingLock.dateExpiresUtc = SystemClock.Instance.GetCurrentInstant().Plus(DefaultExpiryDuration);
                                await _googleGlobalDatastoreRepository.Update(_namespace, existingLock, transaction);
                            }
                            else
                            {
                                _logger?.LogWarning("{0}: Unreleased lock {1}/{2} during renewal appears to have been acquired by someone else!", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                                await _metricService.AddPoint(
                                    LockReleaseMetric,
                                    1,
                                    null,
                                    new Dictionary<string, string>
                                    {
                                        { "namespace", _namespace ?? string.Empty },
                                        { "object_kind", _objectKind },
                                    });

                                // Someone else now owns the lock! Treat it as released from us.
                                _isReleased = true;
                                return;
                            }
                        }

                        _logger?.LogInformation("{0}: Attempting commit of renewal transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                        await _googleGlobalDatastoreRepository.Commit(_namespace, transaction);
                        doRollback = false;
                        _logger?.LogInformation("{0}: Successful commit of renewal transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                        await _metricService.AddPoint(
                            LockRenewedMetric,
                            1,
                            null,
                            new Dictionary<string, string>
                            {
                                { "namespace", _namespace ?? string.Empty },
                                { "object_kind", _objectKind },
                            });
                    }
                    catch (Exception ex)
                    {
                        _logger?.LogCritical(ex, "{0}: Exception during renewal of {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                    }
                    finally
                    {
                        _logger?.LogInformation("{0}: Reached finally block for renewal of {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                        if (doRollback)
                        {
                            _logger?.LogInformation("{0}: Attempting rollback of renewal transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                            await _googleGlobalDatastoreRepository.Rollback(_namespace, transaction);
                            _logger?.LogInformation("{0}: Renewal transaction rollback completed for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                        }
                    }
                }

                _logger?.LogInformation("{0}: Automatic renewal task finished for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
            }

            public async Task Release()
            {
                _logger?.LogInformation("{0}: Starting release of lock {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                _isReleased = true;
                _cancellationTokenSource.Cancel();

                // Wait 1.5 seconds to ensure that we don't cause Datastore contention with ourselves due to short usage
                // of the lock or the renewal just happening (we might still get contention from other processes).
                await Task.Delay(1500);

                _logger?.LogInformation("{0}: Beginning release transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                var transaction = await _googleGlobalDatastoreRepository.BeginTransaction(_namespace);
                _logger?.LogInformation("{0}: Begun release transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                var doRollback = false;
                try
                {
                    _logger?.LogInformation("{0}: Loading existing lock model for {1}/{2} (release)...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                    var existingLock = await _googleGlobalDatastoreRepository.LoadOneBy<DefaultLockModel>(_namespace, _lockKey, transaction);
                    if (existingLock == null)
                    {
                        _logger?.LogWarning("{0}: Unreleased lock {1}/{2} during release appears to have been acquired and released by someone else!", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                        // No lock? Someone else might have already grabbed it and released it (see
                        // the comment in renewal logic).
                        return;
                    }
                    else
                    {
                        // Existing lock, check if we still have the handle on it.
                        if (existingLock.acquisitionGuid == _acquisitionGuid)
                        {
                            _logger?.LogInformation("{0}: Deleting the lock model for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                            // We can explicitly delete the lock because we still own it.
                            await _googleGlobalDatastoreRepository.Delete(_namespace, existingLock, transaction);
                        }
                        else
                        {
                            _logger?.LogWarning("{0}: Unreleased lock {1}/{2} during release appears to have been acquired by someone else!", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                            // Someone else now owns the lock! Treat it as released from us.
                            _isReleased = true;
                            return;
                        }
                    }

                    _logger?.LogInformation("{0}: Attempting commit of release transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                    await _googleGlobalDatastoreRepository.Commit(_namespace, transaction);
                    doRollback = false;
                    _logger?.LogInformation("{0}: Successful commit of release transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());

                    await _metricService.AddPoint(
                        LockReleaseMetric,
                        1,
                        null,
                        new Dictionary<string, string>
                        {
                            { "namespace", _namespace ?? string.Empty },
                            { "object_kind", _objectKind },
                        });
                }
                catch (Exception ex)
                {
                    _logger?.LogCritical(ex, "{0}: Exception during release of {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                    throw;
                }
                finally
                {
                    _logger?.LogInformation("{0}: Reached finally block for release of {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                    if (doRollback)
                    {
                        _logger?.LogInformation("{0}: Attempting rollback of release transaction for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                        await _googleGlobalDatastoreRepository.Rollback(_namespace, transaction);
                        _logger?.LogInformation("{0}: Release transaction rollback completed for {1}/{2}...", _acquisitionGuid, _namespace, _lockKey.GetNameFromKey());
                    }
                }
            }
        }
    }
}
