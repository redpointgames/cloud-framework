﻿using System;
using System.Threading.Tasks;
using Google.Cloud.Datastore.V1;

namespace Redpoint.CloudFramework.Locking
{
    public class DefaultLockService : ILockService
    {
        private readonly ICurrentTenantService _currentTenantService;
        private readonly IGlobalLockService _globalLockService;

        public DefaultLockService(
            ICurrentTenantService currentTenantService,
            IGlobalLockService globalLockService)
        {
            _currentTenantService = currentTenantService;
            _globalLockService = globalLockService;
        }

        public async Task<ILockHandle> Acquire(Key objectToLock)
        {
            var ns = (await _currentTenantService.GetTenant()).DatastoreNamespace;
            return await _globalLockService.Acquire(ns, objectToLock);
        }

        public async Task AcquireAndUse(Key objectToLock, Func<Task> block)
        {
            var ns = (await _currentTenantService.GetTenant()).DatastoreNamespace;
            await _globalLockService.AcquireAndUse(ns, objectToLock, block);
        }

        public async Task<T> AcquireAndUse<T>(Key objectToLock, Func<Task<T>> block)
        {
            var ns = (await _currentTenantService.GetTenant()).DatastoreNamespace;
            return await _globalLockService.AcquireAndUse<T>(ns, objectToLock, block);
        }
    }
}
