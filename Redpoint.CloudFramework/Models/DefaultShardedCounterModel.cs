using System.Collections.Generic;

namespace Redpoint.CloudFramework.Models
{
    public class DefaultShardedCounterModel : Model, IShardedCounterModel
    {
        public long? count { get; set; }

        public override string GetKind()
        {
            return "RCF-SharedCounter";
        }

        public override long GetSchemaVersion()
        {
            return 1;
        }

        public override Dictionary<string, string> GetTypes()
        {
            return new Dictionary<string, string> {
                { "count","integer"}
            };
        }

        public override HashSet<string> GetIndexes()
        {
            return new HashSet<string>
            {
                "count",
            };
        }

        public string GetTypeFieldName()
        {
            return null;
        }

        public string GetCountFieldName()
        {
            return "count";
        }

        public string FormatShardName(string name, int index)
        {
            return string.Format("shard-{0}-{1}", name, index);
        }
    }
}