﻿using Google.Cloud.Datastore.V1;
using NodaTime;
using Redpoint.CloudFramework.Datastore;
using System;
using System.Collections.Generic;

namespace Redpoint.CloudFramework.Models
{
    public abstract class Model
    {
        public Key Key { get; set; }
        public Instant? dateCreatedUtc { get; internal set; }
        public Instant? dateModifiedUtc { get; internal set; }
        public long? schemaVersion { get; set; }
        public bool hasImplicitMigrationsApplied { get; internal set; }

        /// <summary>
        /// The original entity when it was loaded; this is used to clear caches
        /// when appropriate.
        /// </summary>
        internal Dictionary<string, object> _originalData { get; set; }

        public abstract string GetKind();

        public abstract Dictionary<string, string> GetTypes();

        public abstract HashSet<string> GetIndexes();

        public abstract long GetSchemaVersion();

        public virtual MigrationOutcome MigrateFromSchemaVersion(long oldVersion)
        {
            return MigrationOutcome.ImplicitMigrationsApplied;
        }

        public virtual string GetDatastoreNamespaceForLocalKeys()
        {
            throw new NotSupportedException("This model has a property of type 'local-key', but does not implement GetDatastoreNamespaceForLocalKeys");
        }
    }
}
