﻿using NodaTime;
using System.Collections.Generic;

namespace Redpoint.CloudFramework.Models
{
    public class DefaultLockModel : Model
    {
        // The lock key is part of the name in the key.
        public Instant? dateExpiresUtc { get; set; }
        public string acquisitionGuid { get; set; }

        public override string GetKind()
        {
            return "Lock";
        }

        public override long GetSchemaVersion()
        {
            return 1;
        }

        public override Dictionary<string, string> GetTypes()
        {
            return new Dictionary<string, string>
            {
                { "dateExpiresUtc", "timestamp" },
                { "acquisitionGuid", "string" },
            };
        }

        public override HashSet<string> GetIndexes()
        {
            return new HashSet<string>
            {
                "dateExpiresUtc",
                "acquisitionGuid"
            };
        }
    }
}
