﻿using Google.Api.Gax.Grpc;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.Datastore.V1;
using Grpc.Core;
using System.Collections.Generic;

namespace Redpoint.CloudFramework
{
    public interface ICurrentEnvironment
    {
        string GetGoogleProjectId();

        Key GetTenantDatastoreKeyFromNamespace(string @namespace);

        GoogleCredential GetGoogleServiceAccountCredential(IEnumerable<string> scopes);

        ChannelCredentials GetGoogleServiceAccountChannelCredentials(string endpoint, IEnumerable<string> scopes);

        string GetGoogleServiceAccountServiceEndpoint(string endpoint, IEnumerable<string> scopes);

        string GetRedisServerName();

        string GetRedpointPubSubServerName();

        bool GetMetricsEnabled();
    }

    internal static class GoogleBuilder
    {
        internal static TType Build<TType, TBuilder>(ICurrentEnvironment environment, string endpoint, IEnumerable<string> scopes) where TBuilder : ClientBuilderBase<TType>, new()
        {
            var builder = new TBuilder();
            builder.ChannelCredentials = environment.GetGoogleServiceAccountChannelCredentials(endpoint, scopes);
            builder.Endpoint = environment.GetGoogleServiceAccountServiceEndpoint(endpoint, scopes);
            return builder.Build();
        }
    }
}
