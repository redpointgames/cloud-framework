﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Redpoint.CloudFramework.Error;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NodaTime;
using System.Threading;

namespace Redpoint.CloudFramework.Event.PubSub
{
    public class RedpointPubSub : IPubSub
    {
        private readonly ILogger<RedpointPubSub> _logger;
        private readonly IErrorReporting _errorReporting;
        private readonly ICurrentEnvironment _currentEnvironment;

        public RedpointPubSub(
            ILogger<RedpointPubSub> logger,
            IErrorReporting errorReporting,
            ICurrentEnvironment currentEnvironment)
        {
            _logger = logger;
            _errorReporting = errorReporting;
            _currentEnvironment = currentEnvironment;
        }

        public async Task PublishAsync(SerializedEvent jsonObject)
        {
            using (var client = new HttpClient())
            {
                var url = $"http://{_currentEnvironment.GetRedpointPubSubServerName()}:8000/publish";
                await client.PutAsync(url, new StringContent(JsonConvert.SerializeObject(jsonObject)));
            }
        }

        public async Task SubscribeAndLoopUntilCancelled(
            string subscriptionName,
            string[] eventTypes,
            CancellationToken cancellationToken,
            SubscriptionCleanupPolicy cleanupPolicy,
            Func<SerializedEvent, Task<bool>> onMessage)
        {
            var messagePollInProgress = false;
            var lastMessageOperationTime = SystemClock.Instance.GetCurrentInstant();

            while (!cancellationToken.IsCancellationRequested)
            {
                if (messagePollInProgress)
                {
                    continue;
                }

                try
                {
                    messagePollInProgress = true;

                    _logger.LogInformation("Beginning poll for messages...");
                    using (var client = new HttpClient())
                    {
                        var url = $"http://{_currentEnvironment.GetRedpointPubSubServerName()}:8000/poll?clientName=" + WebUtility.UrlEncode(subscriptionName);
                        try
                        {
                            var result = JsonConvert.DeserializeObject<JObject>(await client.GetStringAsync(url));
                            var message = JsonConvert.DeserializeObject<SerializedEvent>(result["message"].Value<string>());
                            if (await onMessage(message))
                            {
                                var ackUrl = $"http://{_currentEnvironment.GetRedpointPubSubServerName()}:8000/ack?clientName=" +
                                             WebUtility.UrlEncode(subscriptionName) + "&messageId=" +
                                             WebUtility.UrlEncode(result["messageId"].ToObject<string>());
                                await client.GetAsync(ackUrl);
                            }

                            lastMessageOperationTime = SystemClock.Instance.GetCurrentInstant();
                            messagePollInProgress = false;
                        }
                        catch (TaskCanceledException ex)
                        {
                            _logger.LogInformation("No messages waiting");
                            messagePollInProgress = false;
                        }
                        catch (HttpRequestException ex)
                        {
                            _errorReporting.Report(ex);
                            messagePollInProgress = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    lastMessageOperationTime = SystemClock.Instance.GetCurrentInstant();
                    messagePollInProgress = false;
                    _errorReporting.Report(ex);
                }

                await Task.Delay(1000);
            }
        }
    }
}
