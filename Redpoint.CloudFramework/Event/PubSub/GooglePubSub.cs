﻿using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using Grpc.Core;
using Redpoint.CloudFramework.Error;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Redpoint.CloudFramework.Metric;
using System.Threading;
using Redpoint.CloudFramework.Prefix;
using System.Linq;

namespace Redpoint.CloudFramework.Event.PubSub
{
    public class GooglePubSub : IPubSub
    {
        private readonly ILogger<GooglePubSub> _logger;
        private readonly IErrorReporting _errorReporting;
        private readonly ICurrentEnvironment _currentEnvironment;
        private readonly IGoogleApiRetry _googleApiRetry;
        private readonly IMetricService _metricService;
        private readonly IGlobalPrefix _globalPrefix;

        private readonly PublisherServiceApiClient _publisherClient;
        private readonly SubscriberServiceApiClient _subscriberClient;
        private readonly ChannelCredentials _publisherCredential;
        private readonly string _publisherServiceEndpoint;
        private readonly ChannelCredentials _subscriberCredential;
        private readonly string _subscriberServiceEndpoint;
        private Dictionary<string, PublisherClient> _publisherClients;
        private readonly SemaphoreSlim _publisherClientCreation;

        private const string GooglePubSubStreamingFailures = "rcf/pubsub_streaming_failure_count";
        private const string GooglePubSubPushCount = "rcf/pubsub_push_count";
        private const string GooglePubSubPullCount = "rcf/pubsub_pull_count";
        private const string GooglePubSubAckCount = "rcf/pubsub_ack_count";
        private const string GooglePubSubNackCount = "rcf/pubsub_nack_count";
        private const string GooglePubSubNackFailCount = "rcf/pubsub_nack_fail_count";

        public GooglePubSub(
            ILogger<GooglePubSub> logger,
            IErrorReporting errorReporting,
            IMetricService metricService,
            ICurrentEnvironment currentEnvironment,
            IGoogleApiRetry googleApiRetry,
            IGlobalPrefix globalPrefix)
        {
            _logger = logger;
            _errorReporting = errorReporting;
            _currentEnvironment = currentEnvironment;
            _googleApiRetry = googleApiRetry;
            _metricService = metricService;
            _globalPrefix = globalPrefix;

            _publisherClient = GoogleBuilder.Build<PublisherServiceApiClient, PublisherServiceApiClientBuilder>(
                _currentEnvironment,
                PublisherServiceApiClient.DefaultEndpoint,
                PublisherServiceApiClient.DefaultScopes);
            _subscriberClient = GoogleBuilder.Build<SubscriberServiceApiClient, SubscriberServiceApiClientBuilder>(
                _currentEnvironment,
                SubscriberServiceApiClient.DefaultEndpoint,
                SubscriberServiceApiClient.DefaultScopes);
            _publisherCredential = _currentEnvironment.GetGoogleServiceAccountChannelCredentials(
                PublisherServiceApiClient.DefaultEndpoint,
                PublisherServiceApiClient.DefaultScopes);
            _publisherServiceEndpoint = _currentEnvironment.GetGoogleServiceAccountServiceEndpoint(
                PublisherServiceApiClient.DefaultEndpoint,
                PublisherServiceApiClient.DefaultScopes);
            _subscriberCredential = _currentEnvironment.GetGoogleServiceAccountChannelCredentials(
                SubscriberServiceApiClient.DefaultEndpoint,
                SubscriberServiceApiClient.DefaultScopes);
            _subscriberServiceEndpoint = _currentEnvironment.GetGoogleServiceAccountServiceEndpoint(
                SubscriberServiceApiClient.DefaultEndpoint,
                SubscriberServiceApiClient.DefaultScopes);
            _publisherClients = new Dictionary<string, PublisherClient>();
            _publisherClientCreation = new SemaphoreSlim(1);
        }

        public async Task PublishAsync(SerializedEvent ser)
        {
            var topicRawName = "event~" + ser.Type.Replace(":", ".");
            var topicName = new TopicName(
                _currentEnvironment.GetGoogleProjectId(),
                topicRawName);

            PublisherClient client;
            await _publisherClientCreation.WaitAsync();
            try
            {
                if (!_publisherClients.ContainsKey(topicRawName))
                {
                    try
                    {
                        // Attempt to create the topic in case it doesn't exist.
                        await _publisherClient.CreateTopicAsync(topicName);
                    }
                    catch (RpcException ex2) when (ex2.Status.StatusCode == StatusCode.AlreadyExists)
                    {
                        // Already exists.
                    }

                    _publisherClients[topicRawName] = await PublisherClient.CreateAsync(
                        topicName,
                        new PublisherClient.ClientCreationSettings(
                            clientCount: 1,
                            publisherServiceApiSettings: null,
                            credentials: _publisherCredential,
                            serviceEndpoint: _publisherServiceEndpoint));
                }

                client = _publisherClients[topicRawName];
            }
            finally
            {
                _publisherClientCreation.Release();
            }

            await client.PublishAsync(new PubsubMessage
            {
                Data = ByteString.CopyFromUtf8(JsonConvert.SerializeObject(ser))
            });

            await _metricService.AddPoint(
                GooglePubSubPushCount,
                1,
                ser.Project == null ? null : _globalPrefix.ParseInternal(string.Empty, ser.Project),
                new Dictionary<string, string>
                {
                    { "event_type", ser.Type },
                    { "entity_type", ser.Key == null ? "(no entity in event)" : _globalPrefix.ParseInternal(string.Empty, ser.Key).Path.Last().Kind },
                });
        }

        public async Task SubscribeAndLoopUntilCancelled(
            string subscriptionName,
            string[] eventTypes,
            CancellationToken baseCancellationToken,
            SubscriptionCleanupPolicy cleanupPolicy,
            Func<SerializedEvent, Task<bool>> onMessage)
        {
            if (baseCancellationToken.IsCancellationRequested)
            {
                return;
            }

            // Subscribe to all of the topics for all of the event types in parallel.
            var cancellationTokenSource = new CancellationTokenSource();
            var cancellationToken = cancellationTokenSource.Token;
            baseCancellationToken.Register(() =>
            {
                cancellationTokenSource.Cancel();
            });
            var tasks = new Task[eventTypes.Length];
            for (var i = 0; i < eventTypes.Length; i++)
            {
                var eventType = eventTypes[i];

                var topicName = new TopicName(
                    _currentEnvironment.GetGoogleProjectId(),
                    "event~" + eventType.Replace(":", "."));
                var subscriberName = new SubscriptionName(
                    _currentEnvironment.GetGoogleProjectId(),
                    subscriptionName + "~" + eventType.Replace(":", "."));

                try
                {
                    await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.PubSub, _logger, async () =>
                    {
                        await _subscriberClient.CreateSubscriptionAsync(
                            subscriberName,
                            topicName,
                            null,
                            600,
                            cancellationToken);
                    });
                }
                catch (RpcException ex) when (ex.Status.StatusCode == StatusCode.NotFound)
                {
                    try
                    {
                        // The topic wasn't found, so create it.
                        await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.PubSub, _logger, async () =>
                        {
                            await _publisherClient.CreateTopicAsync(topicName, cancellationToken);
                        });
                    }
                    catch (RpcException ex2) when (ex2.Status.StatusCode == StatusCode.AlreadyExists)
                    {
                        // Topic has been created in parallel.
                    }

                    try
                    {
                        // Now create the subscription.
                        await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.PubSub, _logger, async () =>
                        {
                            await _subscriberClient.CreateSubscriptionAsync(
                            subscriberName,
                            topicName,
                            null,
                            600,
                            cancellationToken);
                        });
                    }
                    catch (RpcException ex2) when (ex2.Status.StatusCode == StatusCode.AlreadyExists)
                    {
                        // Subscription already exists; everything is OK.
                    }
                }
                catch (RpcException ex) when (ex.Status.StatusCode == StatusCode.AlreadyExists)
                {
                    // Subscription already exists; everything is OK.
                }

                // Set up the task to continously poll.
                tasks[i] = Task.Run(async () =>
                {
                    var simpleSubscriber = await SubscriberClient.CreateAsync(
                        subscriberName,
                        new SubscriberClient.ClientCreationSettings(
                            clientCount: 1,
                            subscriberServiceApiSettings: null,
                            credentials: _subscriberCredential,
                            serviceEndpoint: _subscriberServiceEndpoint),
                        new SubscriberClient.Settings
                        {
                            FlowControlSettings = new Google.Api.Gax.FlowControlSettings(1, null)
                        });
                    cancellationToken.Register(() =>
                    {
                        // Can't return a task here?
                        simpleSubscriber.StopAsync(TimeSpan.FromMinutes(3));
                    });
                    await simpleSubscriber.StartAsync(async (message, cancellationTokenInner) =>
                    {
                        SerializedEvent serializedEvent = null;
                        try
                        {
                            serializedEvent = JsonConvert.DeserializeObject<SerializedEvent>(
                                message.Data.ToStringUtf8());
                            _logger.LogInformation(
                                "Recieved event {eventId} from Google Pub/Sub for {eventType} events from {subscriptionName}",
                                serializedEvent.Id,
                                eventType,
                                subscriptionName);
                            await _metricService.AddPoint(
                                GooglePubSubPullCount,
                                1,
                                serializedEvent.Project == null ? null : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Project),
                                new Dictionary<string, string>
                                {
                                    { "event_type", serializedEvent.Type },
                                    { "subscription_name", subscriptionName },
                                    { "entity_type", serializedEvent.Key == null ? "(no entity in event)" : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Key).Path.Last().Kind },
                                });
                            if (await onMessage(serializedEvent))
                            {
                                await _metricService.AddPoint(
                                    GooglePubSubAckCount,
                                    1,
                                    serializedEvent.Project == null ? null : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Project),
                                    new Dictionary<string, string>
                                    {
                                        { "event_type", serializedEvent.Type },
                                        { "subscription_name", subscriptionName },
                                        { "entity_type", serializedEvent.Key == null ? "(no entity in event)" : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Key).Path.Last().Kind },
                                    });
                                return SubscriberClient.Reply.Ack;
                            }
                            else
                            {
                                await _metricService.AddPoint(
                                    GooglePubSubNackCount,
                                    1,
                                    serializedEvent.Project == null ? null : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Project),
                                    new Dictionary<string, string>
                                    {
                                        { "event_type", serializedEvent.Type },
                                        { "subscription_name", subscriptionName },
                                        { "entity_type", serializedEvent.Key == null ? "(no entity in event)" : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Key).Path.Last().Kind },
                                    });
                                return SubscriberClient.Reply.Nack;
                            }
                        }
                        catch (Exception ex)
                        {
                            _errorReporting.Report(ex);
                            if (serializedEvent != null)
                            {
                                await _metricService.AddPoint(
                                    GooglePubSubNackFailCount,
                                    1,
                                    serializedEvent.Project == null ? null : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Project),
                                    new Dictionary<string, string>
                                    {
                                        { "event_type", serializedEvent.Type },
                                        { "subscription_name", subscriptionName },
                                        { "entity_type", serializedEvent.Key == null ? "(no entity in event)" : _globalPrefix.ParseInternal(string.Empty, serializedEvent.Key).Path.Last().Kind },
                                    });
                            }
                            return SubscriberClient.Reply.Nack;
                        }
                    });
                }, cancellationToken);
            }

            await Task.WhenAny(tasks);

            cancellationTokenSource.Cancel();

            if (cleanupPolicy == SubscriptionCleanupPolicy.DeleteSubscription)
            {
                for (var i = 0; i < eventTypes.Length; i++)
                {
                    var eventType = eventTypes[i];

                    var subscriberName = new SubscriptionName(
                        _currentEnvironment.GetGoogleProjectId(),
                        subscriptionName + "~" + eventType.Replace(":", "."));

                    await _googleApiRetry.DoRetryableOperationAsync(GoogleApiCallContext.PubSub, _logger, async () =>
                    {
                        await _subscriberClient.DeleteSubscriptionAsync(subscriberName);
                    });
                }
            }
        }
    }
}
