﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Event.PubSub
{
    public interface IPubSub
    {
        Task PublishAsync(SerializedEvent jsonObject);

        Task SubscribeAndLoopUntilCancelled(
            string subscriptionName, 
            string[] eventTypes, 
            CancellationToken cancellationToken,
            SubscriptionCleanupPolicy cleanupPolicy,
            Func<SerializedEvent, Task<bool>> onMessage);
    }

    public enum SubscriptionCleanupPolicy
    {
        NoCleanup,
        DeleteSubscription,
    }
}
