﻿using Google.Cloud.Datastore.V1;
using Newtonsoft.Json.Linq;
using NodaTime;

namespace Redpoint.CloudFramework.Event
{
    public class Event
    {
        public Key Id { get; set; }

        public Instant UtcTimestamp { get; set; }

        public string EventType { get; set; }

        public string ServiceIdentifier { get; set; }

        public Key Project { get; set; }

        public Key Session { get; set; }

        public JObject Request { get; set; }

        public Key Key { get; set; }

        public JObject Entity { get; set; }

        public JObject Userdata { get; set; }
    }
}
