﻿using Google.Cloud.Datastore.V1;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Redpoint.CloudFramework.Event
{
    public interface IEventApi
    {
        Task Raise(string eventType, Key project, Key session, HttpRequest request, Key key, object entity,
            object userdata);
    }
}
