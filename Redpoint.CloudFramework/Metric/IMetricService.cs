﻿using Google.Cloud.Datastore.V1;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Redpoint.CloudFramework.Metric
{
    public interface IMetricService
    {
        Task AddPoint(string metricType, int amount, Key projectKey, Dictionary<string, string> labels = null);

        void AddPointSync(string metricType, int amount, Key projectKey, Dictionary<string, string> labels = null);
    }
}
