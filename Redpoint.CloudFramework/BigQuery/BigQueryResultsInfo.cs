﻿using Google.Cloud.BigQuery.V2;

namespace Redpoint.CloudFramework.BigQuery
{
    public class BigQueryResultsInfo
    {
        public BigQueryResults Results { get; set; }

        public BigQueryJob Job { get; set; }
    }
}
