﻿using System.Threading.Tasks;

namespace Redpoint.CloudFramework
{
    public interface ICurrentTenantService
    {
        Task<ICurrentTenant> GetTenant();
    }

    public interface ICurrentTenant
    {
        string DatastoreNamespace { get; }
    }
}
