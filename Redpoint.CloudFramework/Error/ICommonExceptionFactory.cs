﻿using System;

namespace Redpoint.CloudFramework.Error
{
    public interface ICommonExceptionFactory
    {
        Exception IdentifierWrongTypeError(string identifier, string kind);

        Exception IdentifierInvalidError(string identifier, string reason);

        Exception RetryRequestError();

        bool IsRetryRequestError(Exception ex);
    }
}
