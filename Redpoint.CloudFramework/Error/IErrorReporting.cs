﻿using System;

namespace Redpoint.CloudFramework.Error
{
    public interface IErrorReporting
    {
        void Report(Exception ex);
    }
}
