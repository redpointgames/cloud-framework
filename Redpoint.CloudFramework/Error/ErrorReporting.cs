﻿using System;
using Microsoft.Extensions.Logging;
using Sentry;

namespace Redpoint.CloudFramework.Error
{
    public class ErrorReporting : IErrorReporting
    {
        private readonly ILogger<ErrorReporting> _logger;

        public ErrorReporting(ILogger<ErrorReporting> logger)
        {
            _logger = logger;
        }

        public void Report(Exception ex)
        {
            _logger.LogCritical(new EventId(1), ex, ex.Message);

            if (ex is ObjectDisposedException && ex.Message == "Safe handle has been closed")
            {
                // This exception is raised from gRPC calls when the process is shutting down. There's nothing
                // we can do to really fix it (because everything is being torn down), the process can't retry
                // the operation, and logging it to Sentry is useless because we can't act on it. Instead,
                // just return a constant string as the error ID so we know if a request failed because of it.
                return;
            }

            if (ex is InvalidOperationException && ex.Message == "Shutdown has already been called")
            {
                // This exception is raised from gRPC calls when the process is shutting down. There's nothing
                // we can do to really fix it (because everything is being torn down), the process can't retry
                // the operation, and logging it to Sentry is useless because we can't act on it. Instead,
                // just return a constant string as the error ID so we know if a request failed because of it.
                return;
            }

            SentrySdk.CaptureException(ex);
        }
    }
}
